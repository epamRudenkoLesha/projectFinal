package net.polite.deliveryPayment.controller;

import net.polite.deliveryPayment.controller.command.Command;
import net.polite.deliveryPayment.controller.command.CommandEnum;
import net.polite.deliveryPayment.controller.command.CommandsBuilder;
import net.polite.deliveryPayment.controller.servlet.HttpMethodEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.polite.deliveryPayment.view.ViewConstants;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class MainController {
    public static Logger LOG = LogManager.getLogger(MainController.class);

    private Map<String, Map<HttpMethodEnum, Command>> commands;

    private MainController() {
        this.commands = initCommands();
    }

    private static class InstanceHolder {

        private final static MainController INSTANCE = new MainController();
    }

    public static MainController getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public void process(HttpMethodEnum method, HttpServletResponse res, HttpServletRequest req) throws ServletException, IOException {
        Model model = new Model(req.getSession());
        model.setParameters(req.getParameterMap());

        String path = execute(req.getServletPath(), method, model);

        addAttributesToRequest(model, req);
        addCookiesToResponse(model, res);

        if (!redirect(path, res)) {
            req.getRequestDispatcher(path).forward(req, res);
        }
    }


    private void addCookiesToResponse(Model model, HttpServletResponse res) {
        for (Map.Entry<String, String> entry : model.getCookies().entrySet()) {
            Cookie cookie = new Cookie(entry.getKey(), entry.getValue());
            cookie.setMaxAge(86400);
            res.addCookie(cookie);
        }
    }

    private void addAttributesToRequest(Model model, HttpServletRequest req) {
        for (Map.Entry<String, Object> entry : model.getAttributes().entrySet()) {
            req.setAttribute(entry.getKey(), entry.getValue());
        }
    }

    private boolean redirect(String path, HttpServletResponse res) throws IOException {
        if (path.startsWith(ViewConstants.RedirectPrefix)) {
            res.sendRedirect(path.substring(ViewConstants.RedirectPrefix.length() + 1));
            return true;
        }
        return false;
    }

    private String execute(String path, HttpMethodEnum method, Model model) {
        Command command = findCommand(path, method);
        if (command != null) {
            try {
                return command.execute(model);
            } catch (Exception e) {
                LOG.error(e);
                return ViewConstants.Pages.ErrorPage;
            }
        } else {
            LOG.error("Cannot find command for path: " + path);
            return ViewConstants.Pages.Error404Page;
        }
    }

    private Command findCommand(String path, HttpMethodEnum method) {
        Map<HttpMethodEnum, Command> commandMap = commands.get(path);
        return commandMap != null ? commandMap.get(method) : null;
    }

    private Map<String, Map<HttpMethodEnum, Command>> initCommands() {
        Map<String, Map<HttpMethodEnum, Command>> commands = new CommandsBuilder()
                .register(ViewConstants.Paths.HomePage, CommandEnum.HOME_PAGE)
                .register(ViewConstants.Paths.HomePage, HttpMethodEnum.POST, CommandEnum.CALCULATE_RESULT_PAGE)
                .register(ViewConstants.Paths.Login, CommandEnum.LOGIN_PAGE)
                .register(ViewConstants.Paths.Login, HttpMethodEnum.POST, CommandEnum.LOGIN)
                .register(ViewConstants.Paths.Request, HttpMethodEnum.POST, CommandEnum.REQUEST)
                .register(ViewConstants.Paths.PersonalCabinet, CommandEnum.PERSONAL_CABINET)
                .register(ViewConstants.Paths.PersonalCabinet, HttpMethodEnum.POST, CommandEnum.UPDATE_USER_INFO)
                .register(ViewConstants.Paths.Logout, CommandEnum.LOGOUT)
                .register(ViewConstants.Paths.DeleteBill, HttpMethodEnum.POST, CommandEnum.DELETE_BILL)
                .register(ViewConstants.Paths.Bill, CommandEnum.BILL_DETAILS_PAGE)
                .register(ViewConstants.Paths.Bill, HttpMethodEnum.POST, CommandEnum.PAY_BILL)
                .register(ViewConstants.Paths.Register, CommandEnum.REGISTER_PAGE)
                .register(ViewConstants.Paths.Register, HttpMethodEnum.POST, CommandEnum.REGISTER)
                .build();
        return commands;
    }

}
