package net.polite.deliveryPayment.controller;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Model {
    private Map<String, Object> attributes;
    private Map<String, String[]> parameters;
    private Map<String, String> cookies;
    private HttpSession session;

    public Model(HttpSession session) {
        this.session = session;
        this.attributes = new HashMap<>();
        this.parameters = new HashMap<>();
        this.cookies = new HashMap<>();
    }

    public String getParameter(String key) {
        String[] strings = parameters.get(key);
        return Objects.nonNull(strings) && strings.length > 0 ? strings[0] : null;
    }

    public Object getAttribute(String key) {
        return attributes.get(key);
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void addAttribute(String key, Object value) {
        this.attributes.put(key, value);
    }

    public void addSessionAttribute(String key, Object value) {
        this.session.setAttribute(key, value);
    }

    public Map<String, String[]> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String[]> parameters) {
        this.parameters = parameters;
    }

    public Map<String, String> getCookies() {
        return cookies;
    }

    public void setCookies(Map<String, String> cookies) {
        this.cookies = cookies;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }
}
