package net.polite.deliveryPayment.controller.command;

import net.polite.deliveryPayment.controller.Model;

public interface Command {

    String execute(Model model);
}
