package net.polite.deliveryPayment.controller.command;

import net.polite.deliveryPayment.controller.command.impl.*;

public enum CommandEnum {
    HOME_PAGE(new HomePageCommandImpl()),
    CALCULATE_RESULT_PAGE(new CalculateResultPageCommand()),
    LOGIN(new LoginCommand()),
    REQUEST(new CreateRequestCommand()),
    LOGOUT(new LogoutCommand()),
    LOGIN_PAGE(new LoginPageCommand()),
    PERSONAL_CABINET(new PersonalCabinetPageCommandImpl()),
    UPDATE_USER_INFO(new UpdateUserContactInfoCommandImpl()),
    DELETE_BILL(new DeleteBillCommand()),
    BILL_DETAILS_PAGE(new BillDetailsPageCommand()),
    REGISTER_PAGE(new RegisterPageCommand()),
    REGISTER(new RegisterCommand()),
    PAY_BILL(new PayBillCommand());

    private Command command;

    CommandEnum(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }
}
