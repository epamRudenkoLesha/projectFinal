package net.polite.deliveryPayment.controller.command;

import net.polite.deliveryPayment.controller.servlet.HttpMethodEnum;

import java.util.HashMap;
import java.util.Map;

public class CommandsBuilder {

    Map<String, Map<HttpMethodEnum, Command>> commands;

    public CommandsBuilder() {
        this.commands = new HashMap<>();
    }

    public CommandsBuilder register(String path, CommandEnum commandEnum) {
        return register(path, HttpMethodEnum.GET, commandEnum);
    }

    public CommandsBuilder register(String path, HttpMethodEnum method, CommandEnum commandEnum) {
        Map<HttpMethodEnum, Command> commandMap = commands.get(path);
        if (commandMap == null) {
            commandMap = new HashMap<>();
        }
        commandMap.put(method, commandEnum.getCommand());
        commands.put(path, commandMap);
        return this;
    }

    public Map<String, Map<HttpMethodEnum, Command>> build() {
        return this.commands;
    }
}
