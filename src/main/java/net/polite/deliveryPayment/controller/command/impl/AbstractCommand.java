package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.controller.command.Command;
import net.polite.deliveryPayment.model.entities.RoleEnum;
import net.polite.deliveryPayment.model.entities.User;
import net.polite.deliveryPayment.service.impl.ServiceFactory;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.field.Field;
import net.polite.deliveryPayment.view.form.Form;
import net.polite.deliveryPayment.view.form.FormFactory;

import java.util.Objects;

public abstract class AbstractCommand implements Command {

    protected ServiceFactory factory;
    protected FormFactory formFactory;

    public AbstractCommand() {
        this.factory = ServiceFactory.getInstance();
        this.formFactory = FormFactory.getInstance();
    }

    protected void populateForm(Form form, Model model) {
        for (Field field : form.getFields()) {
            String parameter = model.getParameter(field.getName());
            if (Objects.nonNull(parameter)) {
                field.setValue(parameter);
            }
        }
    }

    protected User getUser(Model model) {
        User user = new User();
        user.setName(model.getParameter(ViewConstants.Fields.NAME));
        user.setSurname(model.getParameter(ViewConstants.Fields.SURNAME));
        user.setPatronymic(model.getParameter(ViewConstants.Fields.PATRONYMIC));
        user.setEmail(model.getParameter(ViewConstants.Fields.EMAIL));
        user.setPhone(model.getParameter(ViewConstants.Fields.PHONE));
        user.setPassword(model.getParameter(ViewConstants.Fields.PASSWORD));
        user.addRole(RoleEnum.USER);
        return user;
    }

    protected void populateUserInfoFormFromSession(Form form, Model model) {
        User user = (User) model.getSession().getAttribute(ViewConstants.Attributes.USER);
        form.getFields().get(0).setValue(user.getPhone());
        form.getFields().get(1).setValue(user.getName());
        form.getFields().get(2).setValue(user.getSurname());
        form.getFields().get(3).setValue(user.getPatronymic());
        form.getFields().get(4).setValue(user.getEmail());
    }
}
