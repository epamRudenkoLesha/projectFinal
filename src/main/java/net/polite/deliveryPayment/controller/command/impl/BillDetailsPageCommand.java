package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.model.entities.Bill;
import net.polite.deliveryPayment.service.BillService;
import net.polite.deliveryPayment.service.impl.BillServiceImpl;
import net.polite.deliveryPayment.view.ViewConstants;

public class BillDetailsPageCommand extends AbstractCommand {

    private BillService billService;

    public BillDetailsPageCommand() {
        this.billService = (BillService) factory.<Bill>getService(BillServiceImpl.class);

    }

    @Override
    public String execute(Model model) {
        Bill bill = billService.getItem(Integer.parseInt(model.getParameter(ViewConstants.Attributes.ID)));
        model.addAttribute(ViewConstants.Attributes.BILL, bill);
        return ViewConstants.Pages.Bill;
    }
}
