package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.model.entities.Distance;
import net.polite.deliveryPayment.model.entities.Package;
import net.polite.deliveryPayment.model.entities.PackageType;
import net.polite.deliveryPayment.model.entities.Request;
import net.polite.deliveryPayment.model.entities.User;
import net.polite.deliveryPayment.service.DistanceService;
import net.polite.deliveryPayment.service.PackageTypeService;
import net.polite.deliveryPayment.service.RequestService;
import net.polite.deliveryPayment.service.impl.DistanceServiceImpl;
import net.polite.deliveryPayment.service.impl.PackageTypeServiceImpl;
import net.polite.deliveryPayment.service.impl.RequestServiceImpl;
import net.polite.deliveryPayment.view.SelectEnum;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.form.Form;
import net.polite.deliveryPayment.view.form.FormValidator;

import java.util.Objects;

import static net.polite.deliveryPayment.view.ViewConstants.Fields.WEIGHT;
import static net.polite.deliveryPayment.view.ViewConstants.Fields.CITY_FROM;
import static net.polite.deliveryPayment.view.ViewConstants.Fields.CITY_FROM_COURIER;
import static net.polite.deliveryPayment.view.ViewConstants.Fields.CITY_TO;
import static net.polite.deliveryPayment.view.ViewConstants.Fields.CITY_TO_COURIER;
import static net.polite.deliveryPayment.view.ViewConstants.Fields.PACKAGE_TYPE;

public class CalculateResultPageCommand extends AbstractCommand {

    private PackageTypeService packageTypeService;
    private DistanceService distanceService;
    private RequestService requestService;

    public CalculateResultPageCommand() {
        this.packageTypeService = (PackageTypeService) factory.<PackageType>getService(PackageTypeServiceImpl.class);
        this.distanceService = (DistanceService) factory.<Distance>getService(DistanceServiceImpl.class);
        this.requestService = (RequestService) factory.<Request>getService(RequestServiceImpl.class);
    }

    @Override
    public String execute(Model model) {
        User user = (User) model.getSession().getAttribute(ViewConstants.Attributes.USER);

        Form form;
        if (Objects.nonNull(user)) {
            form = formFactory.getRequestForm();
        } else {
            form = formFactory.getCalculateForm();
        }
        populateForm(form, model);
        FormValidator.validate(form);

        if (!form.getHasErrors()) {
            Request request = getRequest(model);
            int result = requestService.calculateDelivery(request);
            model.addAttribute(ViewConstants.Attributes.RESULT, result);
            model.addAttribute(ViewConstants.Attributes.CALCULATE_AGAIN, true);
        } else {
            form = formFactory.getCalculateForm();
            populateForm(form, model);
            FormValidator.validate(form);
        }
        model.addAttribute(ViewConstants.Attributes.FORM, form);
        return ViewConstants.Pages.HomePage;
    }

    private Request getRequest(Model model) {
        int cityFrom = Integer.parseInt(model.getParameter(CITY_FROM));
        int cityTo = Integer.parseInt(model.getParameter(CITY_TO));
        Distance distance = distanceService.findByCities(cityFrom, cityTo);
        Package aPackage = new Package();
        aPackage.setPackageType(packageTypeService.getItem(Integer.parseInt(model.getParameter(PACKAGE_TYPE))));
        aPackage.setWeight(Integer.parseInt(model.getParameter(WEIGHT)));
        Request request = new Request();
        request.setDistance(distance);
        request.setCourierTo(SelectEnum.valueOf(model.getParameter(CITY_TO_COURIER).toUpperCase()).isValue());
        request.setCourierFrom(SelectEnum.valueOf(model.getParameter(CITY_FROM_COURIER).toUpperCase()).isValue());
        request.setaPackage(aPackage);
        return request;
    }

}
