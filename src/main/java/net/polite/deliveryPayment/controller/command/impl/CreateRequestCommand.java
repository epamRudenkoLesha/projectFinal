package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.Distance;
import net.polite.deliveryPayment.model.entities.Package;
import net.polite.deliveryPayment.model.entities.PackageType;
import net.polite.deliveryPayment.model.entities.Request;
import net.polite.deliveryPayment.model.entities.User;
import net.polite.deliveryPayment.service.DistanceService;
import net.polite.deliveryPayment.service.PackageTypeService;
import net.polite.deliveryPayment.service.RequestService;
import net.polite.deliveryPayment.service.impl.DistanceServiceImpl;
import net.polite.deliveryPayment.service.impl.PackageTypeServiceImpl;
import net.polite.deliveryPayment.service.impl.RequestServiceImpl;
import net.polite.deliveryPayment.view.SelectEnum;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.form.Form;
import net.polite.deliveryPayment.view.form.FormValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static net.polite.deliveryPayment.view.ViewConstants.Fields.*;

public class CreateRequestCommand extends AbstractCommand {

    public static Logger LOG = LogManager.getLogger(CreateRequestCommand.class);

    private PackageTypeService packageTypeService;
    private DistanceService distanceService;
    private RequestService requestService;

    public CreateRequestCommand() {
        this.packageTypeService = (PackageTypeService) factory.<PackageType>getService(PackageTypeServiceImpl.class);
        this.distanceService = (DistanceService) factory.<Distance>getService(DistanceServiceImpl.class);
        this.requestService = (RequestService) factory.<Request>getService(RequestServiceImpl.class);
    }

    @Override
    public String execute(Model model) {
        Form form = formFactory.getRequestForm();
        populateForm(form, model);
        FormValidator.validate(form);

        if (!form.getHasErrors()) {
            try {
                saveRequest(model);
                return ViewConstants.Redirect.PersonalCabinet;
            } catch (QueryException e) {
                LOG.error(e.getMessage(), e);
            }
        }
        model.addAttribute(ViewConstants.Attributes.FORM, form);
        return ViewConstants.Pages.HomePage;

    }

    private void saveRequest(Model model) {
        SimpleDateFormat parser = new SimpleDateFormat(ViewConstants.DateFormat);
        Date deliveryDate = null;
        try {
            deliveryDate = parser.parse(model.getParameter(DELIVERY_DATE));
        } catch (ParseException e) {
            LOG.error("Fail to parse date from model", e);
        }
        User user = (User) model.getSession().getAttribute(ViewConstants.Attributes.USER);
        int cityFrom = Integer.parseInt(model.getParameter(CITY_FROM));
        int cityTo = Integer.parseInt(model.getParameter(CITY_TO));
        Distance distance = distanceService.findByCities(cityFrom, cityTo);
        Package aPackage = new Package();
        aPackage.setPackageType(packageTypeService.getItem(Integer.parseInt(model.getParameter(PACKAGE_TYPE))));
        aPackage.setDeliveryDate(deliveryDate);
        aPackage.setWeight(Integer.parseInt(model.getParameter(WEIGHT)));
        Request request = new Request();
        request.setDistance(distance);
        request.setCourierTo(SelectEnum.valueOf(model.getParameter(CITY_TO_COURIER).toUpperCase()).isValue());
        request.setCourierFrom(SelectEnum.valueOf(model.getParameter(CITY_FROM_COURIER).toUpperCase()).isValue());
        request.setAddressFrom(model.getParameter(CITY_FROM_ADDRESS));
        request.setAddressTo(model.getParameter(CITY_TO_ADDRESS));
        request.setUser(user);
        request.setaPackage(aPackage);
        request.setCost(requestService.calculateDelivery(request));
        requestService.save(request);
    }

}
