package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.model.entities.Bill;
import net.polite.deliveryPayment.service.BillService;
import net.polite.deliveryPayment.service.impl.BillServiceImpl;
import net.polite.deliveryPayment.view.ViewConstants;

public class DeleteBillCommand extends AbstractCommand{

    private BillService billService;

    public DeleteBillCommand() {
        this.billService = (BillService) factory.<Bill>getService(BillServiceImpl.class);
    }

    @Override
    public String execute(Model model) {
        billService.remove(Integer.parseInt(model.getParameter(ViewConstants.Attributes.ID)));
        return ViewConstants.Redirect.PersonalCabinet;
    }
}
