package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.form.Form;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HomePageCommandImpl extends AbstractCommand {

    public static Logger LOG = LogManager.getLogger(HomePageCommandImpl.class);

    @Override
    public String execute(Model model) {
        Form form = formFactory.getCalculateForm();
        model.addAttribute(ViewConstants.Attributes.FORM, form);
        return ViewConstants.Pages.HomePage;
    }
}
