package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.User;
import net.polite.deliveryPayment.service.UserService;
import net.polite.deliveryPayment.service.impl.UserServiceImpl;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.form.Form;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static net.polite.deliveryPayment.view.ViewConstants.Forms.Login.*;

public class LoginCommand extends AbstractCommand {

    public static Logger LOG = LogManager.getLogger(LoginCommand.class);

    public static final String ERROR_WRONG_PHONE_PASSWORD = "error.wrong.phone.password";
    private UserService userService;

    public LoginCommand() {
        this.userService = (UserService) factory.<User>getService(UserServiceImpl.class);
    }

    @Override
    public String execute(Model model) {

        String phone = model.getParameter(ViewConstants.Fields.PHONE);
        String password = model.getParameter(ViewConstants.Fields.PASSWORD);

        String path;
        try {
            User user = userService.getUser(phone, password);
            model.addSessionAttribute(ViewConstants.Attributes.ROLE, userService.getUserRole(user));
            model.addSessionAttribute(ViewConstants.Attributes.USER, user);
            path = ViewConstants.Redirect.HomePage;
        } catch (QueryException e) {
            LOG.error(e.getMessage(), e);
            Form form = formFactory.getLoginForm();
            form.setHasErrors(true);
            form.addError(ERROR_WRONG_PHONE_PASSWORD);
            model.addAttribute(ViewConstants.Attributes.FORM, form);
            path = ViewConstants.Pages.LoginPage;
        }
        return path;
    }


}
