package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.form.Form;

import static net.polite.deliveryPayment.view.ViewConstants.Forms.Login.*;

public class LoginPageCommand extends AbstractCommand {

    @Override
    public String execute(Model model) {
        Form form = formFactory.getLoginForm();
        model.addAttribute(ViewConstants.Attributes.FORM, form);
        return ViewConstants.Pages.LoginPage;
    }
}
