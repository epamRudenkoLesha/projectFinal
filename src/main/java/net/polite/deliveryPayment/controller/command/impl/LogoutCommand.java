package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.model.entities.RoleEnum;
import net.polite.deliveryPayment.view.ViewConstants;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String execute(Model model) {
        model.getSession().removeAttribute(ViewConstants.Attributes.USER);
        model.getSession().setAttribute(ViewConstants.Attributes.ROLE, RoleEnum.ANONYMOUS);
        return ViewConstants.Redirect.HomePage;
    }


}
