package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.model.entities.Bill;
import net.polite.deliveryPayment.service.BillService;
import net.polite.deliveryPayment.service.impl.BillServiceImpl;
import net.polite.deliveryPayment.view.ViewConstants;

import java.util.Date;

public class PayBillCommand extends AbstractCommand{

    private BillService billService;

    public PayBillCommand() {
        this.billService = (BillService) factory.<Bill>getService(BillServiceImpl.class);
    }

    @Override
    public String execute(Model model) {
        Bill bill = billService.getItem(Integer.parseInt(model.getParameter(ViewConstants.Attributes.ID)));
        bill.setPaid(true);
        bill.setPaidDate(new Date());
        billService.update(bill);
        return ViewConstants.Redirect.PersonalCabinet;
    }
}
