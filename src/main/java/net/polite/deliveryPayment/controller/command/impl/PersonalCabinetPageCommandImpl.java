package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.model.entities.Bill;
import net.polite.deliveryPayment.model.entities.Request;
import net.polite.deliveryPayment.model.entities.User;
import net.polite.deliveryPayment.service.BillService;
import net.polite.deliveryPayment.service.RequestService;
import net.polite.deliveryPayment.service.impl.BillServiceImpl;
import net.polite.deliveryPayment.service.impl.RequestServiceImpl;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.form.Form;

public class PersonalCabinetPageCommandImpl extends AbstractCommand {
    private BillService billService;

    public PersonalCabinetPageCommandImpl() {
        this.billService = (BillService) factory.<Bill>getService(BillServiceImpl.class);

    }

    @Override
    public String execute(Model model) {
        Form form = formFactory.getContactInfoForm();
        populateUserInfoFormFromSession(form, model);
        model.addAttribute(ViewConstants.Attributes.FORM, form);
        User user = (User)model.getSession().getAttribute(ViewConstants.Attributes.USER);
        model.addAttribute(ViewConstants.Attributes.BILLS, billService.getAll(user));
        return ViewConstants.Pages.PersonalCabinet;
    }


}
