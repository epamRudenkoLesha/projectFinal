package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.model.entities.User;
import net.polite.deliveryPayment.service.UserService;
import net.polite.deliveryPayment.service.impl.UserServiceImpl;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.form.Form;
import net.polite.deliveryPayment.view.form.FormValidator;

public class RegisterCommand extends AbstractCommand {

    private UserService userService;

    public RegisterCommand() {
        this.userService = (UserService) factory.<User>getService(UserServiceImpl.class);
    }

    @Override
    public String execute(Model model) {
        Form form = formFactory.getRegistrationForm();
        populateForm(form, model);
        FormValidator.validate(form);
        FormValidator.validatePasswords(form);
        FormValidator.validateExistingUser(form);

        if (!form.getHasErrors()) {
            User user = getUser(model);
            user = userService.save(user);
            model.addSessionAttribute(ViewConstants.Attributes.ROLE, userService.getUserRole(user));
            model.addSessionAttribute(ViewConstants.Attributes.USER, user);
            return ViewConstants.Redirect.HomePage;
        } else {
            model.addAttribute(ViewConstants.Attributes.FORM, form);
            return ViewConstants.Pages.Register;
        }
    }

}
