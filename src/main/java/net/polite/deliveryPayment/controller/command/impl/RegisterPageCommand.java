package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.form.Form;

public class RegisterPageCommand extends AbstractCommand {

    @Override
    public String execute(Model model) {
        Form form = formFactory.getRegistrationForm();
        model.addAttribute(ViewConstants.Attributes.FORM, form);
        return ViewConstants.Pages.Register;
    }

}
