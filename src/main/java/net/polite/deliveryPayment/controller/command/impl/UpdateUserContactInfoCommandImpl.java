package net.polite.deliveryPayment.controller.command.impl;

import net.polite.deliveryPayment.controller.Model;
import net.polite.deliveryPayment.model.entities.User;
import net.polite.deliveryPayment.service.UserService;
import net.polite.deliveryPayment.service.impl.UserServiceImpl;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.form.Form;
import net.polite.deliveryPayment.view.form.FormValidator;

public class UpdateUserContactInfoCommandImpl extends PersonalCabinetPageCommandImpl {

    private UserService userService;

    public UpdateUserContactInfoCommandImpl() {
        this.userService = (UserService) factory.<User>getService(UserServiceImpl.class);
    }

    @Override
    public String execute(Model model) {
        Form form = formFactory.getContactInfoForm();
        populateForm(form, model);
        FormValidator.validate(form);
        if (!form.getHasErrors()) {
            User updateUser = getUser(model);
            User modifiedUser = updateCurrentUser(updateUser, model);
            User currentUser = userService.update(modifiedUser);
            model.getSession().setAttribute(ViewConstants.Attributes.USER, currentUser);
        }
        return super.execute(model);
    }

    private User updateCurrentUser(User updateUser, Model model) {
        User currentUser = (User) model.getSession().getAttribute(ViewConstants.Attributes.USER);
        currentUser.setPhone(updateUser.getPhone());
        currentUser.setName(updateUser.getName());
        currentUser.setSurname(updateUser.getSurname());
        currentUser.setPatronymic(updateUser.getPatronymic());
        currentUser.setEmail(updateUser.getEmail());
        return currentUser;
    }


}
