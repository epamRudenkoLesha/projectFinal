package net.polite.deliveryPayment.controller.filter;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * Abstract filter. Enables filter logging and implement obligatory init and destroy Filter methods.
 */
public abstract class AbstractFilter implements Filter {

    /**
     * The Config.
     */
    protected FilterConfig config;
    /**
     * The Context.
     */
    protected ServletContext context;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.config = filterConfig;
        context = config.getServletContext();
    }

    @Override
    public void destroy() {
    }
}
