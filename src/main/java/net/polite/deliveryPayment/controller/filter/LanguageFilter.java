package net.polite.deliveryPayment.controller.filter;

import net.polite.deliveryPayment.utils.LanguageEnum;
import net.polite.deliveryPayment.utils.ResourceManager;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * The Language filter. Manage application localization.
 * Gets language from user cookies, if available.
 * If no cookies available set default language.
 * Check current session lang parameter (change language request). If exists - set as current Locale.
 * If cookie found and it varies from current language - then change language according to cookie value.
 * If cookies found but no lang cookie found - set default value.
 */
@WebFilter(filterName = "languageFilter")
public class LanguageFilter extends AbstractFilter {

    /**
     * Session and cookie parameter LANG.
     */
    public static final String LANG = "lang";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        Cookie[] cookies = req.getCookies();
        String activeLanguage = getCurrentLocale();
        if (Objects.nonNull(cookies) && cookies.length > 0) {
            String parameterLanguage = req.getParameter(LANG);
            Optional<Cookie> cookieOptional = Arrays.stream(cookies).filter(c -> LANG.equals(c.getName())).findFirst();
            if (Objects.nonNull(parameterLanguage)) {
                activeLanguage = updateLocale(resp, parameterLanguage.toLowerCase());
            } else if (cookieOptional.isPresent()) {
                String currentLanguage = getCurrentLocale();
                String cookieLanguage = cookieOptional.get().getValue();
                if (!currentLanguage.equalsIgnoreCase(cookieLanguage)) {
                    try {
                        cookieLanguage = LanguageEnum.valueOf(cookieLanguage).name();
                    } catch (IllegalArgumentException e) {
                        context.log("Language value from cookie is not valid. Setting language to default value");
                        addDefaultLangCookie(resp, getCurrentLocale());
                    }
                    activeLanguage = updateLocale(resp, cookieLanguage);
                }
            } else {
                activeLanguage = getCurrentLocale();
                addDefaultLangCookie(resp, activeLanguage);
            }
        } else {
            activeLanguage = getCurrentLocale();
            addDefaultLangCookie(resp, activeLanguage);
        }
        Config.set(req.getSession(), Config.FMT_LOCALE, ResourceManager.INSTANCE.getActiveLocale());
        req.getSession().setAttribute(LANG, activeLanguage);
        chain.doFilter(req, resp);
    }

    private String getCurrentLocale() {
        return ResourceManager.INSTANCE.getActiveLocale().getLanguage();
    }

    private String updateLocale(HttpServletResponse resp, String parameter) {
        ResourceManager.INSTANCE.changeResource(LanguageEnum.valueOf(parameter.toUpperCase()));
        resp.addCookie(new Cookie(LANG, parameter));
        return parameter;
    }

    private String addDefaultLangCookie(HttpServletResponse response, String activeLanguage) {
        Cookie langCookie = new Cookie(LANG, activeLanguage);
        langCookie.setMaxAge(2592000);
        response.addCookie(langCookie);
        return activeLanguage;
    }

}
