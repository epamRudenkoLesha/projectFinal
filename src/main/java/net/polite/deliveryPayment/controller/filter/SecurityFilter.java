package net.polite.deliveryPayment.controller.filter;

import net.polite.deliveryPayment.model.entities.RoleEnum;
import net.polite.deliveryPayment.view.ViewConstants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

/**
 * Security filter manages application authorization access.
 * By default user has anonymous role. Trying access to restricted pages redirect user to login page.
 */
@WebFilter(filterName = "securityFilter")
public class SecurityFilter extends AbstractFilter {

    /**
     * Map of restricted pages according to user ROLE.
     */
    private HashMap<RoleEnum, List<String>> urls = new HashMap<RoleEnum, List<String>>() {
        {
            put(RoleEnum.ADMIN, ViewConstants.Paths.adminUrls);
            put(RoleEnum.USER, ViewConstants.Paths.userUrls);
            put(RoleEnum.ANONYMOUS, ViewConstants.Paths.anonymousUrls);
        }
    };

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String path = req.getServletPath();
        HttpSession session = req.getSession();
        RoleEnum role = (RoleEnum) session.getAttribute(ViewConstants.Attributes.ROLE);
        if (Objects.isNull(role)) {
            role = RoleEnum.ANONYMOUS;
            session.setAttribute(ViewConstants.Attributes.ROLE, role);
        }
        List<String> accessibleUrls = urls.get(role);
        if (accessibleUrls.contains(path)) {
            res.sendRedirect(ViewConstants.Paths.Login);
        } else {
            chain.doFilter(req, res);
        }
    }

    @Override
    public void destroy() {
        urls = null;
    }
}
