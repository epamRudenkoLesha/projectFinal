package net.polite.deliveryPayment.controller.servlet;

public enum HttpMethodEnum {
    POST,
    GET
}
