package net.polite.deliveryPayment.model.dao;

import net.polite.deliveryPayment.model.entities.Bill;
import net.polite.deliveryPayment.model.entities.User;

import java.util.List;

/**
 * The interface Bill dao.
 */
public interface BillDao extends Dao<Bill> {

    /**
     * Gets all Bills for User.
     *
     * @param user the user
     * @return the all
     */
    List<Bill> getAll(User user);
}
