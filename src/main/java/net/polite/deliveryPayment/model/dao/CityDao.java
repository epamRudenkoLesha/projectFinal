package net.polite.deliveryPayment.model.dao;

import net.polite.deliveryPayment.model.entities.City;

/**
 * The interface City dao.
 */
public interface CityDao extends Dao<City>{
    /**
     * Gets city by iso code.
     *
     * @param isoCode the city iso code
     * @return City
     */
    City getByIsoCode(int isoCode);
}
