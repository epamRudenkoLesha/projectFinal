package net.polite.deliveryPayment.model.dao;

import net.polite.deliveryPayment.model.entities.Item;

import java.util.List;

/**
 * The interface Dao.
 *
 * @param <T> the type parameter
 */
public interface Dao<T extends Item> {
    /**
     * Create item and return operation result - true on success and false on fail
     *
     * @param t the t
     * @return the generated id
     */
    int save(T t);

    /**
     * Remove item and return operation result - true on success and false on fail
     *
     * @param id the id
     * @return the boolean
     */
    void remove (int id);

    /**
     * Update item and return operation result - true on success and false on fail
     *
     * @param t the t
     * @return the boolean
     */
    T update (T t);

    /**
     * Get item by id
     *
     * @param id the id
     * @return the t
     */
    T get(int id);

    /**
     * Gets all items
     *
     * @return the all
     */
    List<T> getAll();

    int getQuantity();
}
