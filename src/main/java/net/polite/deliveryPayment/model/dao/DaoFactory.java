package net.polite.deliveryPayment.model.dao;

import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.exception.FailedFactoryException;
import net.polite.deliveryPayment.model.dao.jdbc.DaoFactoryJDBC;
import net.polite.deliveryPayment.model.entities.Item;
import net.polite.deliveryPayment.utils.PropertiesProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class DaoFactory {

    private static final String DB_DAO_FACTORY_CLASS = "db.dao.factory.class";
    public static Logger LOG = LogManager.getLogger(DaoFactoryJDBC.class);

    public abstract <T extends Item> Dao<T> getDao(Class<? extends Dao> clazz, ConnectionWrapper connection);

    private static volatile DaoFactory INSTANCE;

    public static DaoFactory getInstance() {
        if (INSTANCE == null) {
            synchronized (DaoFactory.class) {
                if (INSTANCE == null) {
                    try {
                        LOG.info("Creating instance of DaoFactory...");
                        String factoryClass = PropertiesProvider.getInstance().getProperty(DB_DAO_FACTORY_CLASS);
                        INSTANCE = (DaoFactory) Class.forName(factoryClass).newInstance();
                    } catch (Exception e) {
                        LOG.error("Error while creating instance of DaoFactory", e);
                        throw new FailedFactoryException(e);
                    }
                }
            }
        }
        return INSTANCE;
    }
}
