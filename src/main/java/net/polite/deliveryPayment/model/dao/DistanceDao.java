package net.polite.deliveryPayment.model.dao;

import net.polite.deliveryPayment.model.entities.Distance;

/**
 * The interface Distance dao.
 */
public interface DistanceDao extends Dao<Distance>{
    /**
     * Find distance by cities iso code.
     *
     * @param cityFrom the cityFrom iso code
     * @param cityTo   the cityTo iso code
     * @return the distance
     */
    Distance findByCitiesIso(int cityFrom, int cityTo);
}
