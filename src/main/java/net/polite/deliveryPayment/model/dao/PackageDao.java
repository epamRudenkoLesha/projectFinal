package net.polite.deliveryPayment.model.dao;

import net.polite.deliveryPayment.model.entities.Package;

/**
 * The interface Package dao.
 */
public interface PackageDao extends Dao<Package>{
}
