package net.polite.deliveryPayment.model.dao;

import net.polite.deliveryPayment.model.entities.PackageType;

/**
 * The interface Package type dao.
 */
public interface PackageTypeDao extends Dao<PackageType>{
}
