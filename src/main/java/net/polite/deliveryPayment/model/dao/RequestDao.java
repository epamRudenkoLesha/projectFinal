package net.polite.deliveryPayment.model.dao;

import net.polite.deliveryPayment.model.entities.Request;
import net.polite.deliveryPayment.model.entities.User;

import java.util.List;

/**
 * The interface Request dao.
 */
public interface RequestDao extends Dao<Request> {


}
