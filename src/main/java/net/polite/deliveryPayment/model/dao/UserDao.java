package net.polite.deliveryPayment.model.dao;

import net.polite.deliveryPayment.model.entities.RoleEnum;
import net.polite.deliveryPayment.model.entities.User;

import java.util.List;

/**
 * The interface User dao.
 */
public interface UserDao extends Dao<User>{

    List<Integer> getUserRoles(int id);

    User findUser(String phone);

    void addRoleToUser(User user, RoleEnum role);
}
