package net.polite.deliveryPayment.model.dao.connection;

import java.sql.Connection;

/**
 * The interface Connection wrapper.
 */
public interface ConnectionWrapper extends AutoCloseable {

    /**
     * Begin transaction.
     */
    void beginTransaction();

    /**
     * Commit transaction.
     */
    void commitTransaction();

    /**
     * Rollback transaction.
     */
    void rollbackTransaction();

    void close();

    /**
     * Gets connection.
     *
     * @return the connection
     */
    public Connection getConnection();

}
