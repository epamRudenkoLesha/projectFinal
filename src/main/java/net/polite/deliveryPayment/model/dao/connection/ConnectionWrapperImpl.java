package net.polite.deliveryPayment.model.dao.connection;

import net.polite.deliveryPayment.model.exceptions.ConnectionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * The type Connection wrapper.
 */
public class ConnectionWrapperImpl implements ConnectionWrapper {

    private static final Logger LOG = LogManager.getLogger(ConnectionWrapperImpl.class);
    private Connection connection;
    private boolean inTransaction = false;

    /**
     * Instantiates a new Connection wrapper.
     *
     * @param connection the connection
     */
    public ConnectionWrapperImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void beginTransaction() {
        try {
            connection.setAutoCommit(false);
            inTransaction = true;
            LOG.info("Transaction started...");
        } catch (SQLException e) {
            LOG.error("Error while beginning transaction ", e);
            throw new ConnectionException(e);
        }
    }

    @Override
    public void commitTransaction() {
        try {
            connection.commit();
            connection.setAutoCommit(true);
            inTransaction = false;
            LOG.info("Transaction was committed...");
        } catch (SQLException e) {
            LOG.error("Error while committing transaction ", e);
            throw new ConnectionException(e);
        }
    }

    @Override
    public void rollbackTransaction() {
        try {
            connection.rollback();
            connection.setAutoCommit(true);
            inTransaction = false;
            LOG.info("Transaction was rolled back... ");
        } catch (SQLException e) {
            LOG.error("Error while rolling back transaction ", e);
            throw new ConnectionException(e);
        }
    }

    @Override
    public void close() {
        try {
            if (inTransaction) {
                rollbackTransaction();
            }
            connection.close();
            LOG.info("Connection was closed...");
        } catch (SQLException e) {
            LOG.error("Error while closing transaction ", e);
            throw new ConnectionException(e);
        }
    }

    @Override
    public Connection getConnection() {
        LOG.info("Getting connection...");
        return connection;
    }
}
