package net.polite.deliveryPayment.model.dao.exception;

public class FailedFactoryException extends RuntimeException {
    public FailedFactoryException(Throwable cause) {
        super(cause);
    }
}
