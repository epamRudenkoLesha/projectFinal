package net.polite.deliveryPayment.model.dao.exception;

public class QueryException extends RuntimeException {

    public QueryException(String message, Exception e) {
        super(message,e);
    }

    public QueryException(String message) {
        super(message);
    }
}
