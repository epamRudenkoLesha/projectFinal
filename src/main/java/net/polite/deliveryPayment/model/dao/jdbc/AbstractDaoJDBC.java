package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.Dao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class AbstractDaoJDBC<T extends Item> implements Dao<T> {

    public static Logger LOG = LogManager.getLogger(AbstractDaoJDBC.class);

    protected static final String INSERT = "INSERT INTO ";
    protected static final String UPDATE = "UPDATE ";
    protected static final String SELECT = "SELECT ";
    protected static final String DELETE = "DELETE ";
    protected static final String FROM = " FROM  ";
    protected static final String SET = " SET ";
    protected static final String AND = " AND ";
    protected static final String WHERE = " WHERE ";
    protected static final String JOIN = " JOIN ";
    protected static final String ON = " ON ";
    protected static final String VALUES = " VALUES ";
    protected static final String GET_QUERY = SELECT + "*" + FROM;
    protected ConnectionWrapper connection;

    protected AbstractDaoJDBC() {
    }

    @Override
    public int save(T t) {
        String query = INSERT + getTableName() + " (" + getInsertFields() + ")" + VALUES + "(" + getInsertParamsFields() + ")";
        try (PreparedStatement preparedStatement = connection.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS)) {
            fillInsertParams(preparedStatement, t);
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new QueryException("No id for item was generated");
            }
        } catch (SQLException e) {
            getLOG().error("Fail to insert item", e);
            throw new QueryException("Fail to insert item", e);
        }
    }

    @Override
    public T update(T t) {
        String query = UPDATE + getTableName() + SET + getUpdateParams(t)
                + WHERE + ConstantsJDBC.Item.ID + "=" + t.getId();
        try (PreparedStatement statement = connection.getConnection().prepareStatement(query)) {
            fillUpdateParams(statement, t);
            int rawsEffected = statement.executeUpdate();
            if (rawsEffected == 1) {
                return t;
            } else {
                throw new QueryException("fail to update item " + t);
            }
        } catch (SQLException e) {
            getLOG().error("Fail to update item", e);
            throw new QueryException("Fail to update item " + t.getClass(), e);
        }
    }

    protected void fillUpdateParams(PreparedStatement statement, T t) throws SQLException {

    }

    @Override
    public void remove(int id) {
        String query = DELETE + FROM + getTableName() + WHERE + ConstantsJDBC.Item.ID + "=?";
        try (PreparedStatement preparedStatement = connection.getConnection().prepareStatement(query)) {
            preparedStatement.setInt(1, id);
            int i = preparedStatement.executeUpdate();
            if (i != 1) {
                throw new QueryException("No item was deleted.");
            }
        } catch (SQLException e) {
            getLOG().error("Fail to delete item", e);
            throw new QueryException("Fail to delete item", e);
        }
    }

    @Override
    public T get(int id) {
        T result = getItemByParameter(id, ConstantsJDBC.Item.ID);
        return result;
    }

    @Override
    public List<T> getAll() {
        List<T> result = new ArrayList<>();
        try (Statement statement = connection.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(GET_QUERY + getTableName());
            while (resultSet.next()) {
                result.add(getItem(resultSet));
            }
        } catch (SQLException e) {
            getLOG().error(e);
        }
        return result;
    }

    @Override
    public int getQuantity() {
        int result = 0;
        try (Statement statement = connection.getConnection().createStatement()) {
            ResultSet resultSet = statement.executeQuery(SELECT + "COUNT(*)"+ FROM + getTableName());
            if (resultSet.next()) {
                result = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            getLOG().error(e);
        }
        return result;
    }

    public void setConnection(ConnectionWrapper connection) {
        this.connection = connection;
    }

    protected String specialSymbolReplacement(String name) {
        name = name.replace("\\", "\\\\");
        name = name.replace("\"", "\\\"");
        return name.replace("\'", "\\\'");
    }

    protected String getUpdateParams(T t) {
        return "";
    }

    protected T getItemByParameter(Object value, String parameter) throws QueryException {
        T result = null;
        String query = GET_QUERY + getTableName() + WHERE + parameter + "=?";
        try (PreparedStatement preparedStatement = connection.getConnection().prepareStatement(query)) {
            preparedStatement.setObject(1, value);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = getItem(resultSet);
            } else {
                getLOG().error("No {} found by {} with value {}.", getTableName(), parameter, value);
                throw new QueryException("No " + getTableName() + " found by " + parameter + " with value " + value);
            }
        } catch (SQLException e) {
            getLOG().error(e);
            throw new QueryException("Fail to find " + getTableName() + " by " + parameter + " with value " + value, e);
        }
        return result;
    }

    protected abstract String getTableName();

    protected abstract T getItem(ResultSet resultSet) throws SQLException;

    protected String getParametersQueryString(List<String> parameters) {
        StringBuilder queryInsertion = new StringBuilder();
        for (int i = 0; i < parameters.size(); i++) {
            if (i == parameters.size() - 1) {
                queryInsertion.append(parameters.get(i));
            } else {
                queryInsertion.append(parameters.get(i)).append(',');
            }
        }
        return queryInsertion.toString();
    }

    private String getInsertParamsFields() {
        Stream<String> stringStream = Stream.iterate("?", e -> e = "?");
        return getParametersQueryString(stringStream.limit(getInsertQuantityFields()).collect(Collectors.toList()));
    }

    protected int getInsertQuantityFields() {
        return 0;
    }

    protected String getInsertFields() {
        return getParametersQueryString(Collections.emptyList());
    }

    protected void fillInsertParams(PreparedStatement preparedStatement, T t) throws SQLException {
    }

    protected Logger getLOG() {
        return this.LOG;
    }
}
