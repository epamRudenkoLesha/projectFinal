package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.BillDao;
import net.polite.deliveryPayment.model.entities.*;
import net.polite.deliveryPayment.model.entities.Package;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static net.polite.deliveryPayment.model.dao.jdbc.ConstantsJDBC.Request.*;
import static net.polite.deliveryPayment.model.dao.jdbc.ConstantsJDBC.Bill.*;

public class BillDaoJDBC extends AbstractDaoJDBC<Bill> implements BillDao {

    public static Logger LOG = LogManager.getLogger(BillDaoJDBC.class);

    @Override
    public List<Bill> getAll(User user) {
        String query = GET_QUERY + getTableName() + WHERE + USER + "=?";
        List<Bill> result = new ArrayList<>();
        try (PreparedStatement statement = connection.getConnection().prepareStatement(query)) {
            statement.setInt(1, user.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(getItem(resultSet));
            }
        } catch (SQLException e) {
            LOG.error(e);
        }
        return result;
    }

    @Override
    protected Bill getItem(ResultSet resultSet) throws SQLException {
        try {
            int id = resultSet.getInt(ID);
            boolean courierFrom = resultSet.getBoolean(COURIER_FROM);
            boolean courierTo = resultSet.getBoolean(COURIER_TO);
            String addressFrom = resultSet.getString(ADDRESS_FROM);
            String addressTo = resultSet.getString(ADDRESS_TO);
            Distance distance = new Distance(resultSet.getInt(DISTANCE));
            Package aPackage = new Package(resultSet.getInt(PACKAGE));
            User user = new User(resultSet.getInt(USER));
            int cost = resultSet.getInt(COST);
            Date paidDate = resultSet.getDate(PAID_DATE);
            boolean paid = resultSet.getBoolean(PAID);
            return new Bill(id, distance, aPackage, user, courierFrom, courierTo, addressFrom, addressTo, cost, paidDate, paid);
        } catch (SQLException e) {
            LOG.error(e);
        }
        return null;
    }

    @Override
    protected String getUpdateParams(Bill bill) {
        List<String> params = new ArrayList<>();
        params.add(ConstantsJDBC.Bill.FIELDS[0] + "=?");
        params.add(ConstantsJDBC.Bill.FIELDS[1] + "=?");
        return getParametersQueryString(params);
    }

    @Override
    protected void fillUpdateParams(PreparedStatement preparedStatement, Bill bill) throws SQLException {
        preparedStatement.setBoolean(1, bill.isPaid());
        preparedStatement.setDate(2, new java.sql.Date(bill.getPaidDate().getTime()));
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }
}
