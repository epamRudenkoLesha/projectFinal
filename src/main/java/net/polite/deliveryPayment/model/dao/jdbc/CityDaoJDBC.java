package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.CityDao;
import net.polite.deliveryPayment.model.entities.City;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;

import static net.polite.deliveryPayment.model.dao.jdbc.ConstantsJDBC.City.*;

public class CityDaoJDBC extends AbstractDaoJDBC<City> implements CityDao {

    public static Logger LOG = LogManager.getLogger(CityDaoJDBC.class);

    CityDaoJDBC() {
    }

    @Override
    protected City getItem(ResultSet resultSet) {
        try {
            int id = resultSet.getInt(ID);
            int isoCode = resultSet.getInt(ISO_CODE);
            int tarif = resultSet.getInt(COURIER_TARIF);
            String codeValue = resultSet.getString(CODE_VALUE);
            return new City(id, codeValue, isoCode, tarif);
        } catch (SQLException e) {
            LOG.error(e);
        }
        return null;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public City getByIsoCode(int isoCode) {
        return getItemByParameter(isoCode, ISO_CODE);
    }
}
