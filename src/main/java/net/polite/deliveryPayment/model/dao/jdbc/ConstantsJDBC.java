package net.polite.deliveryPayment.model.dao.jdbc;

public interface ConstantsJDBC {

    interface Item {
        String ID = "ID";
    }

    interface City {
        String TABLE_NAME = "CITY";
        String ID = Item.ID;
        String CODE_VALUE = "CODE_VALUE";
        String ISO_CODE = "ISO_CODE";
        String COURIER_TARIF = "COURIER_TARIF";
    }

    interface Distance {
        String TABLE_NAME = "DISTANCE";
        String ID = Item.ID;
        String DISTANCE = "DISTANCE";
        String CITY_ISO_FROM = "CITY_ISO_FROM";
        String CITY_ISO_TO = "CITY_ISO_TO";
    }

    interface PackageType {
        String TABLE_NAME = "PACKAGE_TYPE";
        String ID = Item.ID;
        String CODE_VALUE = "CODE_VALUE";
        String TARIFF = "TARIFF";
    }

    interface Package {
        String TABLE_NAME = "PACKAGE";
        String ID = Item.ID;
        String PACKAGE_TYPE = "PACKAGE_TYPE_ID";
        String WEIGHT = "WEIGHT";
        String DELIVERY_DATE = "DELIVERY_DATE";
        String[] FIELDS = {PACKAGE_TYPE, WEIGHT, DELIVERY_DATE};

    }

    interface User {
        String TABLE_NAME = "USER";
        String ID = Item.ID;
        String NAME = "NAME";
        String SURNAME = "SURNAME";
        String PATRONYMIC = "PATRONYMIC";
        String EMAIL = "EMAIL";
        String PHONE = "PHONE";
        String PASSWORD = "PASS";
        String[] FIELDS = {NAME, SURNAME, PATRONYMIC, EMAIL, PHONE, PASSWORD};
    }

    interface Request {
        String TABLE_NAME = "REQUEST";
        String ID = Item.ID;
        String DISTANCE = "DISTANCE_ID";
        String PACKAGE = "PACKAGE_ID";
        String USER = "USER_ID";
        String COST = "COST";
        String COURIER_FROM = "CITY_TO_COURIER";
        String COURIER_TO = "CITY_FROM_COURIER";
        String ADDRESS_FROM = "CITY_FROM_ADDRESS";
        String ADDRESS_TO = "CITY_TO_ADDRESS";
        String[] FIELDS = {DISTANCE, PACKAGE, USER, COURIER_FROM, COURIER_TO, ADDRESS_FROM, ADDRESS_TO, COST};
    }

    interface Bill {
        String PAID_DATE = "PAID_DATE";
        String PAID = "PAID";
        String[] FIELDS = {PAID, PAID_DATE};
    }

    interface Roles {
        String TABLE_NAME = "ROLE";
        String ROLE_ID = "ROLE_ID";
        String USER_ID = "USER_ID";
        String[] FIELDS = {USER_ID, ROLE_ID};

    }

}
