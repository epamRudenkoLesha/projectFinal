package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.Dao;
import net.polite.deliveryPayment.model.dao.DaoFactory;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.entities.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DaoFactoryJDBC extends DaoFactory {

    public static Logger LOG = LogManager.getLogger(DaoFactoryJDBC.class);

    private DaoFactoryJDBC() {
    }

    private static class InstanceHolder {
        private static DaoFactoryJDBC INSTANCE = new DaoFactoryJDBC();
    }

    public <T extends Item> AbstractDaoJDBC<T> getDao(Class<? extends Dao> clazz, ConnectionWrapper connection) {
        AbstractDaoJDBC<T> dao = null;
        try {
            dao = (AbstractDaoJDBC<T>) clazz.newInstance();
            dao.setConnection(connection);
        } catch (IllegalAccessException | InstantiationException e) {
            LOG.error(e);
        }
        return dao;
    }

    public static DaoFactoryJDBC getInstance() {
        return InstanceHolder.INSTANCE;
    }
}
