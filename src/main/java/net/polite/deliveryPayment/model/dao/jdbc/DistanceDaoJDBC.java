package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.DistanceDao;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.City;
import net.polite.deliveryPayment.model.entities.Distance;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static net.polite.deliveryPayment.model.dao.jdbc.ConstantsJDBC.Distance.*;

public class DistanceDaoJDBC extends AbstractDaoJDBC<Distance> implements DistanceDao {

    public static Logger LOG = LogManager.getLogger(DistanceDaoJDBC.class);
    String FIND_BY_CITIES_ISO = GET_QUERY + getTableName() + WHERE + CITY_ISO_FROM + "=?" + AND + CITY_ISO_TO + "=?";

    DistanceDaoJDBC() {
    }

    @Override
    public Distance findByCitiesIso(int cityFrom, int cityTo) {
        Distance result = null;
        try (PreparedStatement preparedStatement = connection.getConnection().prepareStatement(FIND_BY_CITIES_ISO)) {
            preparedStatement.setInt(1, cityFrom);
            preparedStatement.setInt(2, cityTo);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = getItem(resultSet);

            } else {
                LOG.error("No {} found by isoCodeFrom {} and isoCodeTo {}.", getTableName(), cityFrom, cityTo);
                throw new QueryException("No " + getTableName() + " found by isoCodeFrom " + cityFrom + " and isoCodeTo " + cityTo);
            }
        } catch (SQLException e) {
            LOG.error(e);
            throw new QueryException("Fail to find " + getTableName() + " by isoCodeFrom " + cityFrom + " and isoCodeTo " + cityTo, e);
        }

        return result;
    }

    @Override
    protected Distance getItem(ResultSet resultSet) {
        try {
            int id = resultSet.getInt(ID);
            City cityFrom = new City(resultSet.getInt(CITY_ISO_FROM));
            City cityTo = new City(resultSet.getInt(CITY_ISO_TO));
            int distance = resultSet.getInt(DISTANCE);
            return new Distance(id, distance, cityFrom, cityTo);
        } catch (SQLException e) {
            LOG.error(e);
        }
        return null;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }
}
