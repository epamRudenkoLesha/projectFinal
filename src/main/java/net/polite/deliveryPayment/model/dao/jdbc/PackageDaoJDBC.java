package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.PackageDao;
import net.polite.deliveryPayment.model.entities.Package;
import net.polite.deliveryPayment.model.entities.PackageType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

import static net.polite.deliveryPayment.model.dao.jdbc.ConstantsJDBC.Package.*;

public class PackageDaoJDBC extends AbstractDaoJDBC<Package> implements PackageDao {

    public static Logger LOG = LogManager.getLogger(PackageDaoJDBC.class);

    PackageDaoJDBC() {
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected Package getItem(ResultSet resultSet) {
        try {
            int id = resultSet.getInt(ID);
            int weight = resultSet.getInt(WEIGHT);
            Date deliveryDate = resultSet.getDate(DELIVERY_DATE);
            PackageType packageType = new PackageType(resultSet.getInt(PACKAGE_TYPE));
            return new Package(id, weight, deliveryDate, packageType);
        } catch (SQLException e) {
            LOG.error(e);
        }
        return null;
    }

    @Override
    protected void fillInsertParams(PreparedStatement preparedStatement, Package aPackage) throws SQLException {
        preparedStatement.setInt(1, aPackage.getPackageType().getId());
        preparedStatement.setInt(2, aPackage.getWeight());
        preparedStatement.setTimestamp(3, new Timestamp(aPackage.getDeliveryDate().getTime()));
    }

    @Override
    protected int getInsertQuantityFields() {
        return ConstantsJDBC.Package.FIELDS.length;
    }

    @Override
    protected String getInsertFields() {
        return getParametersQueryString(Arrays.asList(ConstantsJDBC.Package.FIELDS));
    }
}
