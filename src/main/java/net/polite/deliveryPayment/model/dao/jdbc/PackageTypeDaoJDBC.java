package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.PackageTypeDao;
import net.polite.deliveryPayment.model.entities.PackageType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;

import static net.polite.deliveryPayment.model.dao.jdbc.ConstantsJDBC.PackageType.*;

public class PackageTypeDaoJDBC extends AbstractDaoJDBC<PackageType> implements PackageTypeDao {

    public static Logger LOG = LogManager.getLogger(PackageTypeDaoJDBC.class);

    PackageTypeDaoJDBC() {
    }

    @Override
    protected PackageType getItem(ResultSet resultSet) {
        try {
            int id = resultSet.getInt(ID);
            int tariff = resultSet.getInt(TARIFF);
            String codeValue = resultSet.getString(CODE_VALUE);
            return new PackageType(id, tariff, codeValue);
        } catch (SQLException e) {
            LOG.error(e);
        }
        return null;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected int getInsertQuantityFields() {
        return 0;
    }

    @Override
    protected String getInsertFields() {
        return null;
    }
}
