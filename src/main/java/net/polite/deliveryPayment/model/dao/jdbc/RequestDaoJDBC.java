package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.RequestDao;
import net.polite.deliveryPayment.model.entities.Distance;
import net.polite.deliveryPayment.model.entities.Package;
import net.polite.deliveryPayment.model.entities.Request;
import net.polite.deliveryPayment.model.entities.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static net.polite.deliveryPayment.model.dao.jdbc.ConstantsJDBC.Request.*;

public class RequestDaoJDBC extends AbstractDaoJDBC<Request> implements RequestDao {

    public static Logger LOG = LogManager.getLogger(RequestDaoJDBC.class);

    @Override
    protected Request getItem(ResultSet resultSet) throws SQLException {
        try {
            int id = resultSet.getInt(ID);
            boolean courierFrom = resultSet.getBoolean(COURIER_FROM);
            boolean courierTo = resultSet.getBoolean(COURIER_TO);
            String addressFrom = resultSet.getString(ADDRESS_FROM);
            String addressTo = resultSet.getString(ADDRESS_TO);
            Distance distance = new Distance(resultSet.getInt(DISTANCE));
            Package aPackage = new Package(resultSet.getInt(PACKAGE));
            User user = new User(resultSet.getInt(USER));
            int cost = resultSet.getInt(COST);
            return new Request(id, distance, aPackage, user, courierFrom, courierTo, addressFrom, addressTo, cost);
        } catch (SQLException e) {
            LOG.error(e);
        }
        return null;
    }

    @Override
    protected void fillInsertParams(PreparedStatement preparedStatement, Request request) throws SQLException {
        preparedStatement.setInt(1, request.getDistance().getId());
        preparedStatement.setInt(2, request.getaPackage().getId());
        preparedStatement.setInt(3, request.getUser().getId());
        preparedStatement.setBoolean(4, request.isCourierFrom());
        preparedStatement.setBoolean(5, request.isCourierTo());
        preparedStatement.setString(6, specialSymbolReplacement(request.getAddressFrom()));
        preparedStatement.setString(7, specialSymbolReplacement(request.getAddressTo()));
        preparedStatement.setInt(8, request.getCost());
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected int getInsertQuantityFields() {
        return ConstantsJDBC.Request.FIELDS.length;
    }

    @Override
    protected String getInsertFields() {
        return getParametersQueryString(Arrays.asList(ConstantsJDBC.Request.FIELDS));
    }
}
