package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.UserDao;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.RoleEnum;
import net.polite.deliveryPayment.model.entities.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ParameterizedMessage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static net.polite.deliveryPayment.model.dao.jdbc.ConstantsJDBC.User.*;

public class UserDaoJDBC extends AbstractDaoJDBC<User> implements UserDao {

    public static Logger LOG = LogManager.getLogger(UserDaoJDBC.class);

    private final static String FIND_ROLES = GET_QUERY + ConstantsJDBC.Roles.TABLE_NAME +
            JOIN + TABLE_NAME + ON + ID + "=" + ConstantsJDBC.Roles.USER_ID +
            WHERE + ID + "=?";

    @Override
    public User findUser(String phone) {
        return getItemByParameter(phone, PHONE);
    }

    @Override
    public void addRoleToUser(User user, RoleEnum role) {
        String insertRoleQuery = INSERT + ConstantsJDBC.Roles.TABLE_NAME + "(" + getParametersQueryString(Arrays.asList(ConstantsJDBC.Roles.FIELDS)) + ")"
                + VALUES + "(?,?)";

        try (PreparedStatement preparedStatement = connection.getConnection().prepareStatement(insertRoleQuery)) {
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setInt(2, role.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOG.error(new ParameterizedMessage("Fail to insert role {} for user id {}", role, user.getId()) ,e);
            throw new QueryException("Fail to insert role " + role + " for user with id " + user.getId(), e);
        }

    }

    @Override
    public List<Integer> getUserRoles(int id) {
        List<Integer> result = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.getConnection().prepareStatement(FIND_ROLES)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                result.add(resultSet.getInt(ConstantsJDBC.Roles.ROLE_ID));
            }
        } catch (SQLException e) {
            LOG.error(e);
        }
        return result;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected void fillInsertParams(PreparedStatement preparedStatement, User user) throws SQLException {
        preparedStatement.setString(1, specialSymbolReplacement(user.getName()));
        preparedStatement.setString(2, specialSymbolReplacement(user.getSurname()));
        preparedStatement.setString(3, specialSymbolReplacement(user.getPatronymic()));
        preparedStatement.setString(4, user.getEmail());
        preparedStatement.setString(5, user.getPhone());
        preparedStatement.setString(6, user.getPassword());
    }

    @Override
    protected void fillUpdateParams(PreparedStatement preparedStatement, User user) throws SQLException {
        preparedStatement.setString(1, specialSymbolReplacement(user.getName()));
        preparedStatement.setString(2, specialSymbolReplacement(user.getSurname()));
        preparedStatement.setString(3, specialSymbolReplacement(user.getPatronymic()));
        preparedStatement.setString(4, user.getEmail());
        preparedStatement.setString(5, user.getPhone());
    }

    @Override
    protected String getUpdateParams(User user) {
        List<String> params = new ArrayList<>();
        params.add(ConstantsJDBC.User.FIELDS[0] + "=?");
        params.add(ConstantsJDBC.User.FIELDS[1] + "=?");
        params.add(ConstantsJDBC.User.FIELDS[2] + "=?");
        params.add(ConstantsJDBC.User.FIELDS[3] + "=?");
        params.add(ConstantsJDBC.User.FIELDS[4] + "=?");
        return getParametersQueryString(params);
    }

    @Override
    protected User getItem(ResultSet resultSet) throws SQLException {
        try {
            int id = resultSet.getInt(ID);
            String name = resultSet.getString(NAME);
            String surname = resultSet.getString(SURNAME);
            String patronymic = resultSet.getString(PATRONYMIC);
            String email = resultSet.getString(EMAIL);
            String phone = resultSet.getString(PHONE);
            String password = resultSet.getString(PASSWORD);
            return new User(id, name, surname, patronymic, email, phone, password);
        } catch (SQLException e) {
            LOG.error(e);
        }
        return null;
    }

    @Override
    protected int getInsertQuantityFields() {
        return ConstantsJDBC.User.FIELDS.length;
    }

    @Override
    protected String getInsertFields() {
        return getParametersQueryString(Arrays.asList(ConstantsJDBC.User.FIELDS));
    }
}
