package net.polite.deliveryPayment.model.entities;

import java.io.Serializable;
import java.util.Date;

public class Bill extends Request implements Serializable {

    private Date paidDate;
    private boolean paid;

    public Bill() {
    }

    public Bill(int id) {
        super(id);
    }

    public Bill(int id, Distance distance, Package aPackage, User user, boolean courierFrom, boolean courierTo, String addressFrom, String addressTo, int cost, Date paidDate, boolean paid) {
        super(id, distance, aPackage, user, courierFrom, courierTo, addressFrom, addressTo, cost);
        this.paidDate = paidDate;
        this.paid = paid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Bill bill = (Bill) o;

        return paidDate != null ? paidDate.equals(bill.paidDate) : bill.paidDate == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (paidDate != null ? paidDate.hashCode() : 0);
        return result;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date paidDate) {
        this.paidDate = paidDate;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }
}
