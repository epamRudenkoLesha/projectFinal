package net.polite.deliveryPayment.model.entities;

public class City extends Item {
    private String name;
    private String code;
    private int isoCode;
    private int courierTarif;

    public City() {
    }

    public City(int isoCode) {
        this.isoCode = isoCode;
    }

    public City(int id, String code, int isoCode, int courierTarif) {
        super(id);
        this.code = code;
        this.isoCode = isoCode;
        this.courierTarif = courierTarif;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        if (isoCode != city.isoCode) return false;
        return code != null ? code.equals(city.code) : city.code == null;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + isoCode;
        return result;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(int isoCode) {
        this.isoCode = isoCode;
    }

    public int getCourierTarif() {
        return courierTarif;
    }

    public void setCourierTarif(int courierTarif) {
        this.courierTarif = courierTarif;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
