package net.polite.deliveryPayment.model.entities;

public class Distance extends Item {

    private int distance;
    private City cityFrom;
    private City cityTo;

    public Distance() {
    }

    public Distance(int id) {
        super(id);
    }

    public Distance(int id, int distance, City cityFrom, City cityTo) {
        super(id);
        this.distance = distance;
        this.cityFrom = cityFrom;
        this.cityTo = cityTo;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public City getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(City cityFrom) {
        this.cityFrom = cityFrom;
    }

    public City getCityTo() {
        return cityTo;
    }

    public void setCityTo(City cityTo) {
        this.cityTo = cityTo;
    }
}
