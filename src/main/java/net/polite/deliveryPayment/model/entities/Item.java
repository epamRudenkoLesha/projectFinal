package net.polite.deliveryPayment.model.entities;

import java.io.Serializable;

public abstract class Item implements Serializable {

    private int id;

    protected Item() {
    }

    protected Item(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return id == item.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
