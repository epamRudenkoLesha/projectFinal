package net.polite.deliveryPayment.model.entities;

import java.util.Date;

public class Package extends Item {

    private int weight;
    private Date deliveryDate;
    private PackageType packageType;

    public Package() {
    }

    public Package(int id) {
        super(id);
    }

    public Package(int id, int weight, Date deliveryDate, PackageType packageType) {
        super(id);
        this.weight = weight;
        this.deliveryDate = deliveryDate;
        this.packageType = packageType;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public PackageType getPackageType() {
        return packageType;
    }

    public void setPackageType(PackageType packageType) {
        this.packageType = packageType;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
}
