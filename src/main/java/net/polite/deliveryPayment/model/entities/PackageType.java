package net.polite.deliveryPayment.model.entities;

public class PackageType extends Item {

    private int tariff;
    private String name;
    private String codeValue;

    public PackageType() {
    }

    public PackageType(int id, int tariff, String codeValue) {
        super(id);
        this.tariff = tariff;
        this.codeValue = codeValue;
    }

    public PackageType(int id) {
        super(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        PackageType that = (PackageType) o;

        if (tariff != that.tariff) return false;
        return codeValue != null ? codeValue.equals(that.codeValue) : that.codeValue == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + tariff;
        result = 31 * result + (codeValue != null ? codeValue.hashCode() : 0);
        return result;
    }

    public int getTariff() {
        return tariff;
    }

    public void setTariff(int tariff) {
        this.tariff = tariff;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeValue() {
        return codeValue;
    }

    public void setCodeValue(String codeValue) {
        this.codeValue = codeValue;
    }
}
