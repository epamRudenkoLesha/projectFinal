package net.polite.deliveryPayment.model.entities;

public class Request extends Item {

    private Distance distance;
    private Package aPackage;
    private User user;
    private boolean courierFrom;
    private boolean courierTo;
    private String addressFrom;
    private String addressTo;
    private int cost;

    public Request() {
    }

    public Request(int id) {
        super(id);
    }

    public Request(int id, Distance distance, Package aPackage, User user,
                   boolean courierFrom, boolean courierTo, String addressFrom, String addressTo, int cost) {
        super(id);
        this.distance = distance;
        this.aPackage = aPackage;
        this.user = user;
        this.courierFrom = courierFrom;
        this.courierTo = courierTo;
        this.addressFrom = addressFrom;
        this.addressTo = addressTo;
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Request request = (Request) o;

        if (!distance.equals(request.distance)) return false;
        if (!aPackage.equals(request.aPackage)) return false;
        return user.equals(request.user);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + distance.hashCode();
        result = 31 * result + aPackage.hashCode();
        result = 31 * result + user.hashCode();
        return result;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isCourierFrom() {
        return courierFrom;
    }

    public void setCourierFrom(boolean courierFrom) {
        this.courierFrom = courierFrom;
    }

    public boolean isCourierTo() {
        return courierTo;
    }

    public void setCourierTo(boolean courierTo) {
        this.courierTo = courierTo;
    }

    public String getAddressFrom() {
        return addressFrom;
    }

    public void setAddressFrom(String addressFrom) {
        this.addressFrom = addressFrom;
    }

    public String getAddressTo() {
        return addressTo;
    }

    public void setAddressTo(String addressTo) {
        this.addressTo = addressTo;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
