package net.polite.deliveryPayment.model.entities;

/**
 * The Role enum.
 */
public enum RoleEnum {

    /**
     * Admin role.
     */
    ADMIN(1),
    /**
     * User role.
     */
    USER(2),
    /**
     * Anonymous role.
     */
    ANONYMOUS(3);

    RoleEnum(int id) {
        this.id = id;
    }

    private int id;

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }
}
