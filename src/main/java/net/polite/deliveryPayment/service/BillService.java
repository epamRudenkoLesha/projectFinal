package net.polite.deliveryPayment.service;

import net.polite.deliveryPayment.model.entities.Bill;
import net.polite.deliveryPayment.model.entities.User;

import java.util.List;

public interface BillService extends Service<Bill>{

    List<Bill> getAll(User user);

}
