package net.polite.deliveryPayment.service;

import net.polite.deliveryPayment.model.entities.City;

public interface CityService extends Service<City>{

    City getCityByIsoCode(int isoCode);
}
