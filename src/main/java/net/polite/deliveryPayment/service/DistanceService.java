package net.polite.deliveryPayment.service;

import net.polite.deliveryPayment.model.entities.Distance;

public interface DistanceService extends Service<Distance>{

    Distance findByCities(int cityFrom, int cityTo);

}
