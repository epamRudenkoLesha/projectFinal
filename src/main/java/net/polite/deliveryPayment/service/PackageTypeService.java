package net.polite.deliveryPayment.service;

import net.polite.deliveryPayment.model.entities.PackageType;

public interface PackageTypeService extends Service<PackageType>{

}
