package net.polite.deliveryPayment.service;

import net.polite.deliveryPayment.model.entities.Request;

public interface RequestService extends Service<Request>{

    int calculateDelivery(Request request);
}
