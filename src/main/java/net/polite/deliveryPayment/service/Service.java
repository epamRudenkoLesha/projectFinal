package net.polite.deliveryPayment.service;

import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;

import java.util.List;

public interface Service<T> {
    T save(T t);
    T update(T t);
    void remove(int id);
    T getItem(int id);
    List<T> getAll();
    void fillItem(T t, ConnectionWrapper connection);
}
