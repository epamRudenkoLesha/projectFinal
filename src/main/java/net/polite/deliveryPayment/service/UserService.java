package net.polite.deliveryPayment.service;

import net.polite.deliveryPayment.model.entities.RoleEnum;
import net.polite.deliveryPayment.model.entities.User;

public interface UserService extends Service<User>{

    User getUser(String phone, String password);

    RoleEnum getUserRole(User user);
}
