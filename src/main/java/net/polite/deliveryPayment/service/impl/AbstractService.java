package net.polite.deliveryPayment.service.impl;

import net.polite.deliveryPayment.model.dao.Dao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapperImpl;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.dao.jdbc.DaoFactoryJDBC;
import net.polite.deliveryPayment.model.entities.Item;
import net.polite.deliveryPayment.service.Service;
import net.polite.deliveryPayment.utils.DBUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.util.List;

public abstract class AbstractService<T extends Item> implements Service<T> {

    public static Logger LOG = LogManager.getLogger(AbstractService.class);

    protected DaoFactoryJDBC factory;

    public AbstractService() {
        this.factory = DaoFactoryJDBC.getInstance();
    }

    @Override
    public T save(T t) {
        prepareItem(t);
        ConnectionWrapper connection = getConnection();
        try {
            connection.beginTransaction();
            createDependencies(t, connection);
            t.setId(getDao(connection).save(t));
            attachDependencies(t, connection);
            connection.commitTransaction();
        } catch (QueryException e) {
            connection.rollbackTransaction();
            throw new QueryException(e.getMessage());
        } finally {
            connection.close();
        }
        return t;
    }

    @Override
    public T update(T t) {
        T update = null;
        ConnectionWrapper connection = getConnection();
        try {
            connection.beginTransaction();
            update = getDao(connection).update(t);
            connection.commitTransaction();
        } catch (QueryException e) {
            getLOG().error("Fail updating item {} in db.", t);
            connection.rollbackTransaction();
        } finally {
            connection.close();
        }
        return update;
    }

    @Override
    public T getItem(int id) {
        T t;
        try (ConnectionWrapper connection = getConnection()) {
            t = getDao(connection).get(id);
            fillItem(t, connection);
        }
        return t;
    }

    @Override
    public List<T> getAll() {
        List<T> all;
        try (ConnectionWrapper connection = getConnection()) {
            all = getDao(connection).getAll();
            for (T item : all) {
                fillItem(item, connection);
            }
        }
        return all;
    }

    @Override
    public void remove(int id) {
        try (ConnectionWrapper connection = getConnection()) {
            getDao(connection).remove(id);
        }
    }

    protected final ConnectionWrapper getConnection() {
        Connection connection = DBUtils.getConnection();
        return new ConnectionWrapperImpl(connection);
    }

    protected Logger getLOG() {
        return this.LOG;
    }

    protected void createDependencies(T t, ConnectionWrapper connection) {
    }

    protected void attachDependencies(T t, ConnectionWrapper connection) {
    }

    public void fillItem(T t, ConnectionWrapper connection) {
    }

    protected void prepareItem(T t) {
    }

    protected abstract Dao<T> getDao(ConnectionWrapper connection);
}
