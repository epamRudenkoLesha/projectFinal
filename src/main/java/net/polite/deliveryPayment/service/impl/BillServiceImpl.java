package net.polite.deliveryPayment.service.impl;

import net.polite.deliveryPayment.model.dao.*;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.jdbc.*;
import net.polite.deliveryPayment.model.entities.*;
import net.polite.deliveryPayment.service.BillService;
import net.polite.deliveryPayment.service.RequestService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class BillServiceImpl extends AbstractService<Bill> implements BillService {

    public static Logger LOG = LogManager.getLogger(BillServiceImpl.class);
    private RequestService requestService;

    public BillServiceImpl() {
        this.requestService = (RequestService) ServiceFactory.getInstance().<Request>getService(RequestServiceImpl.class);
    }

    @Override
    protected Dao<Bill> getDao(ConnectionWrapper connection) {
        return factory.getDao(BillDaoJDBC.class, connection);
    }

    @Override
    public List<Bill> getAll(User user) {
        List<Bill> all;
        try (ConnectionWrapper connection = getConnection()) {
            all = ((BillDao)getDao(connection)).getAll(user);
            for (Bill item : all) {
                fillItem(item, connection);
            }
        }
        return all;
    }

    @Override
    public void fillItem(Bill bill, ConnectionWrapper connection) {
        requestService.fillItem(bill, connection);
    }
}
