package net.polite.deliveryPayment.service.impl;

import net.polite.deliveryPayment.model.dao.CityDao;
import net.polite.deliveryPayment.model.dao.Dao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.jdbc.CityDaoJDBC;
import net.polite.deliveryPayment.model.entities.City;
import net.polite.deliveryPayment.service.CityService;
import net.polite.deliveryPayment.utils.ResourceManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CityServiceImpl extends AbstractService<City> implements CityService {

    public static Logger LOG = LogManager.getLogger(CityServiceImpl.class);

    @Override
    public City getCityByIsoCode(int isoCode) {
        City city;
        try (ConnectionWrapper connection = getConnection()) {
            city = ((CityDao)getDao(connection)).getByIsoCode(isoCode);
            fillItem(city, connection);
        }
        return city;
    }

    @Override
    public void fillItem(City city, ConnectionWrapper connection) {
        String code = city.getCode();
        city.setName(ResourceManager.INSTANCE.getString(code));
    }

    @Override
    protected Dao<City> getDao(ConnectionWrapper connection) {
        return factory.getDao(CityDaoJDBC.class, connection);
    }
}
