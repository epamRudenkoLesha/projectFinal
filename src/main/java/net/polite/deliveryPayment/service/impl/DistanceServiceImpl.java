package net.polite.deliveryPayment.service.impl;

import net.polite.deliveryPayment.model.dao.DistanceDao;
import net.polite.deliveryPayment.model.dao.Dao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.jdbc.DistanceDaoJDBC;
import net.polite.deliveryPayment.model.entities.City;
import net.polite.deliveryPayment.model.entities.Distance;
import net.polite.deliveryPayment.service.CityService;
import net.polite.deliveryPayment.service.DistanceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DistanceServiceImpl extends AbstractService<Distance> implements DistanceService {

    public static Logger LOG = LogManager.getLogger(DistanceServiceImpl.class);

    public CityService cityService;

    public DistanceServiceImpl() {
        this.cityService = (CityService) ServiceFactory.getInstance().<City>getService(CityServiceImpl.class);
    }

    @Override
    public Distance findByCities(int cityFrom, int cityTo) {
        ConnectionWrapper connection = getConnection();
        Distance distance = ((DistanceDao) getDao(connection)).findByCitiesIso(cityFrom, cityTo);
        fillItem(distance, connection);
        connection.close();
        return distance;
    }

    @Override
    public void fillItem(Distance distance, ConnectionWrapper connection) {
        City cityFrom = cityService.getCityByIsoCode(distance.getCityFrom().getIsoCode());
        City cityTo = cityService.getCityByIsoCode(distance.getCityTo().getIsoCode());
        distance.setCityFrom(cityFrom);
        distance.setCityTo(cityTo);
    }

    @Override
    protected Dao<Distance> getDao(ConnectionWrapper connection) {
        return factory.getDao(DistanceDaoJDBC.class, connection);
    }
}
