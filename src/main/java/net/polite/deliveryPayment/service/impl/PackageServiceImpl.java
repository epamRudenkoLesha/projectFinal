package net.polite.deliveryPayment.service.impl;

import net.polite.deliveryPayment.model.dao.Dao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.jdbc.PackageDaoJDBC;
import net.polite.deliveryPayment.model.entities.Package;
import net.polite.deliveryPayment.model.entities.PackageType;
import net.polite.deliveryPayment.service.PackageService;
import net.polite.deliveryPayment.service.PackageTypeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PackageServiceImpl extends AbstractService<Package> implements PackageService {

    public static Logger LOG = LogManager.getLogger(PackageServiceImpl.class);
    private PackageTypeService packageTypeService;

    public PackageServiceImpl() {
        this.packageTypeService = (PackageTypeService) ServiceFactory.getInstance().<PackageType>getService(PackageTypeServiceImpl.class);

    }

    @Override
    protected Dao<Package> getDao(ConnectionWrapper connection) {
        return factory.getDao(PackageDaoJDBC.class, connection);
    }

    @Override
    public void fillItem(Package aPackage, ConnectionWrapper connection) {
        aPackage.setPackageType(packageTypeService.getItem(aPackage.getPackageType().getId()));
    }
}
