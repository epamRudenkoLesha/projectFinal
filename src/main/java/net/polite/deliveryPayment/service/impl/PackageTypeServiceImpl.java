package net.polite.deliveryPayment.service.impl;

import net.polite.deliveryPayment.model.dao.Dao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.jdbc.PackageTypeDaoJDBC;
import net.polite.deliveryPayment.model.entities.PackageType;
import net.polite.deliveryPayment.service.PackageTypeService;
import net.polite.deliveryPayment.utils.ResourceManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PackageTypeServiceImpl extends AbstractService<PackageType> implements PackageTypeService {

    public static Logger LOG = LogManager.getLogger(PackageTypeServiceImpl.class);

    @Override
    protected Dao<PackageType> getDao(ConnectionWrapper connection) {
        return factory.getDao(PackageTypeDaoJDBC.class, connection);
    }

    @Override
    public void fillItem(PackageType packageType, ConnectionWrapper connection) {
        String codeValue = packageType.getCodeValue();
        packageType.setName(ResourceManager.INSTANCE.getString(codeValue));
    }
}
