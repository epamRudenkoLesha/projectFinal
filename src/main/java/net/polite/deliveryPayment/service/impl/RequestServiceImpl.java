package net.polite.deliveryPayment.service.impl;

import net.polite.deliveryPayment.model.dao.*;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.jdbc.PackageDaoJDBC;
import net.polite.deliveryPayment.model.dao.jdbc.RequestDaoJDBC;
import net.polite.deliveryPayment.model.entities.*;
import net.polite.deliveryPayment.model.entities.Package;
import net.polite.deliveryPayment.service.*;
import net.polite.deliveryPayment.utils.PropertiesProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RequestServiceImpl extends AbstractService<Request> implements RequestService {

    public static Logger LOG = LogManager.getLogger(RequestServiceImpl.class);
    private PackageService packageService;
    private DistanceService distanceService;
    private UserService userService;


    public RequestServiceImpl() {
        this.packageService = (PackageService) ServiceFactory.getInstance().<Package>getService(PackageServiceImpl.class);
        this.distanceService = (DistanceService) ServiceFactory.getInstance().<Distance>getService(DistanceServiceImpl.class);
        this.userService = (UserService) ServiceFactory.getInstance().<User>getService(UserServiceImpl.class);
    }

    @Override
    public int calculateDelivery(Request request) {
        double cityFromCourier = request.isCourierFrom() ? request.getDistance().getCityFrom().getCourierTarif() / 100 : 0;
        double cityToCourier = request.isCourierTo() ? request.getDistance().getCityTo().getCourierTarif() / 100 : 0;
        double distanceTarif = Double.parseDouble(PropertiesProvider.getInstance().getProperty("distance.tarif"));
        double distanceCoef = request.getDistance().getDistance() * distanceTarif;
        double packageCoef = request.getaPackage().getPackageType().getTariff() / 100;
        double weightCoef = request.getaPackage().getWeight() / 1000.0;
        BigDecimal bd = new BigDecimal((cityFromCourier + cityToCourier + distanceCoef * weightCoef / 100) * packageCoef);
        bd = bd.setScale(0, RoundingMode.HALF_UP);
        return bd.intValue();
    }

    @Override
    public void fillItem(Request aRequest, ConnectionWrapper connection) {
        aRequest.setaPackage(packageService.getItem(aRequest.getaPackage().getId()));
        aRequest.setUser(userService.getItem(aRequest.getUser().getId()));
        aRequest.setDistance(distanceService.getItem(aRequest.getDistance().getId()));
    }

    @Override
    protected void createDependencies(Request request, ConnectionWrapper connection) {
        PackageDao packageDao = (PackageDao) factory.<Package>getDao(PackageDaoJDBC.class, connection);
        request.getaPackage().setId(packageDao.save(request.getaPackage()));
    }

    @Override
    protected Dao<Request> getDao(ConnectionWrapper connection) {
        return factory.getDao(RequestDaoJDBC.class, connection);
    }
}
