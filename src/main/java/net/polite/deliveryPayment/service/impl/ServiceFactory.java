package net.polite.deliveryPayment.service.impl;

import net.polite.deliveryPayment.model.entities.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ServiceFactory {

    public static Logger LOG = LogManager.getLogger(ServiceFactory.class);

    private static ServiceFactory factory;

    private ServiceFactory() {
    }

    public static ServiceFactory getInstance() {
        synchronized (ServiceFactory.class) {
            if (factory == null) {
                factory = new ServiceFactory();
            }
            return factory;
        }
    }

    public<T extends Item> AbstractService<T> getService(Class<? extends AbstractService> clazz) {
        AbstractService<T> service = null;
        try {
            service = (AbstractService<T>) clazz.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            LOG.error(e);
        }
        return service;
    }
}
