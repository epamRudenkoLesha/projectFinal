package net.polite.deliveryPayment.service.impl;

import net.polite.deliveryPayment.model.dao.Dao;
import net.polite.deliveryPayment.model.dao.UserDao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.dao.jdbc.UserDaoJDBC;
import net.polite.deliveryPayment.model.entities.RoleEnum;
import net.polite.deliveryPayment.model.entities.User;
import net.polite.deliveryPayment.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.util.password.BasicPasswordEncryptor;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class UserServiceImpl extends AbstractService<User> implements UserService {

    public static Logger LOG = LogManager.getLogger(UserServiceImpl.class);

    @Override
    public User getUser(String phone, String password) {
        BasicPasswordEncryptor passwordDecryptor = new BasicPasswordEncryptor();
        ConnectionWrapper connection = getConnection();
        User user = ((UserDao) getDao(connection)).findUser(phone);
        fillItem(user, connection);
        if (passwordDecryptor.checkPassword(password, user.getPassword())) {
            return user;
        } else {
            throw new QueryException("No user with specified phone or|and password.");
        }
    }

    @Override
    public RoleEnum getUserRole(User user) {
        user.getRoles().sort(Comparator.comparingInt(RoleEnum::getId));
        return user.getRoles().get(0);
    }

    @Override
    public void fillItem(User user, ConnectionWrapper connection) {
        List<Integer> rolesId = ((UserDao) getDao(connection)).getUserRoles(user.getId());
        List<RoleEnum> roleEnums = rolesId.stream().map(id -> RoleEnum.values()[id - 1]).collect(Collectors.toList());
        user.setRoles(roleEnums);
    }

    @Override
    protected void attachDependencies(User user, ConnectionWrapper connection) {
        List<RoleEnum> roles = user.getRoles();
        for (RoleEnum role : roles) {
            ((UserDao) getDao(connection)).addRoleToUser(user, role);
        }
    }

    @Override
    protected Dao<User> getDao(ConnectionWrapper connection) {
        return factory.getDao(UserDaoJDBC.class, connection);
    }

    @Override
    protected void prepareItem(User user) {
        String password = user.getPassword();
        BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
        user.setPassword(passwordEncryptor.encryptPassword(password));
    }
}
