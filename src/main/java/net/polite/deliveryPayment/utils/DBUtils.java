package net.polite.deliveryPayment.utils;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.taglibs.standard.tag.common.sql.DataSourceWrapper;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;

/**
 * Instantiate connection to DB with default connection pool on server side using
 * Provides additional initialization of DB on start in @initializeDB() method
 */
public class DBUtils {

    /**
     * The constant LOG.
     */
    public static Logger LOG = LogManager.getLogger(DBUtils.class);

    private static DataSourceWrapper ds;
    private static Properties projectProperties = PropertiesProvider.getInstance().getProperties();

    static {
        ds = new DataSourceWrapper();
        try {
            ds.setJdbcURL(projectProperties.getProperty("db.url"));
            ds.setDriverClassName(projectProperties.getProperty("db.driverClassName"));
            ds.setUserName(projectProperties.getProperty("db.userName"));
            ds.setPassword(projectProperties.getProperty("db.password"));
            String initOnStart = projectProperties.getProperty("db.prod.initialize.onStart");
            if (Boolean.valueOf(initOnStart)) {
                initializeDB();
            }
        } catch (IllegalAccessException e) {
            LOG.error("Access error to DB driver class.", e);
        } catch (InstantiationException e) {
            LOG.error("Driver instantiation error.", e);
        } catch (ClassNotFoundException e) {
            LOG.error("DB driver not found.", e);
        }
    }

    /**
     * Gets connection.
     *
     * @return the connection to DB
     */
    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = ds.getConnection();
        } catch (SQLException e) {
            LOG.error("No connection to DB! Check server config or/and server state (should be running).", e);
        }
        return connection;
    }

    /**
     * DB Initialization/Population method
     */
    private static void initializeDB() {
        ScriptRunner runner = new ScriptRunner(getConnection());
        runScriptFromFile(runner, projectProperties.getProperty("db.prod.initialize.file"));
        runScriptFromFile(runner, projectProperties.getProperty("db.prod.populate.file"));
    }


    /**
     * Run script from file for on start initialization/population
     * @param runner
     * @param fileName
     */
    private static void runScriptFromFile(ScriptRunner runner, String fileName) {
        InputStream populateStream = DBUtils.class.getClassLoader().getResourceAsStream(fileName);
        if (!Objects.isNull(populateStream)) {
            runner.runScript(new BufferedReader(new InputStreamReader(populateStream)));
        } else {
            LOG.error("No file {} found. Pls check proper name.", fileName);
        }
    }
}
