package net.polite.deliveryPayment.utils;

/**
 * All handled project Languages.
 */
public enum LanguageEnum {
    /**
     * Ukr language.
     */
    UKR,
    /**
     * Rus language.
     */
    RUS,
    /**
     * Eng language.
     */
    ENG
}
