package net.polite.deliveryPayment.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.polite.deliveryPayment.service.impl.ServiceFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Properties provider for getting access to instantiated properties in resources/project.properties file.
 */
public class PropertiesProvider {

    private static volatile PropertiesProvider instance;

    /**
     * Logger
     */
    public static Logger LOG = LogManager.getLogger(ServiceFactory.class);
    private Properties properties;

    private PropertiesProvider() {
        properties = new Properties();
        try (InputStream propsFile = this.getClass().getClassLoader().getResourceAsStream("project.properties")) {
            properties.load(propsFile);
        } catch (IOException e) {
            LOG.fatal(e);
        }
    }

    /**
     * Gets PropertiesProvider instance.
     *
     * @return the instance
     */
    public static PropertiesProvider getInstance() {
        if (instance == null) {
            synchronized (PropertiesProvider.class) {
                if (instance == null) {
                    instance = new PropertiesProvider();
                }
            }
        }
        return instance;
    }

    /**
     * Gets all properties.
     *
     * @return the properties
     */
    public Properties getProperties() {
        return this.properties;
    }

    /**
     * Gets property.
     *
     * @param key the key
     * @return the property
     */
    public String getProperty(String key) {
        return properties.getProperty(key);
    }

}
