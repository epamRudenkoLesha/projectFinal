package net.polite.deliveryPayment.utils;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * The Resource manager that manage localized resource bundles.
 */
public enum ResourceManager {

    /**
     * Instance resource manager.
     */
    INSTANCE;

    private ResourceBundle resourceBundle;
    private Locale locale;
    private final String resourceName = PropertiesProvider.getInstance().getProperty("resource.bundle.file");

    ResourceManager() {
        changeResource(LanguageEnum.UKR);
    }

    /**
     * Change resource bundle according to new LanguageEnum
     *
     * @param language the language enum value
     */
    public void changeResource(LanguageEnum language) {
        this.locale = new Locale(language.name().toLowerCase());
        resourceBundle = ResourceBundle.getBundle(resourceName, locale);
    }

    /**
     * Gets localized property value from active resource bundle.
     *
     * @param key the key
     * @return localized property value
     */
    public String getString(String key) {
        return resourceBundle.getString(key);
    }

    /**
     * Gets active locale.
     *
     * @return the active locale
     */
    public Locale getActiveLocale() {
        return this.locale;
    }
}
