package net.polite.deliveryPayment.view;

import net.polite.deliveryPayment.utils.ResourceManager;

public enum SelectEnum {
    YES("yes", "select.yes", true, 2),
    NO("no", "select.no", false, 1);

    SelectEnum(String code, String bundleCode, boolean value, int id) {
        this.code = code;
        this.bundleCode = bundleCode;
        this.value = value;
        this.id = id;
    }

    private String code;
    private String bundleCode;
    private boolean value;
    private int id;

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public String getLocalizedName() {
        return ResourceManager.INSTANCE.getString(bundleCode);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
