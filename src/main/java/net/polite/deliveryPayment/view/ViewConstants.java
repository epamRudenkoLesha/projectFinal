package net.polite.deliveryPayment.view;

import net.polite.deliveryPayment.controller.servlet.HttpMethodEnum;

import java.util.ArrayList;
import java.util.List;

import static net.polite.deliveryPayment.view.ViewConstants.Fields.*;

public interface ViewConstants {

    String DateFormat = "yyyy-MM-dd";
    String RedirectPrefix = "redirect:";
    String JSP = "/WEB-INF/jsp/";

    interface Pages {
        String HomePage = JSP + "homePage.jsp";
        String LoginPage = JSP + "login.jsp";
        String Register = JSP + "register.jsp";
        String Bill = JSP + "billDetails.jsp";
        String ErrorPage = JSP + "error.jsp";
        String Error404Page = JSP + "error_404.jsp";
        String PersonalCabinet = JSP + "personalCabinet.jsp";
    }

    interface Redirect {
        String HomePage = RedirectPrefix + Paths.HomePage;
        String PersonalCabinet = RedirectPrefix + Paths.PersonalCabinet;
    }

    interface Paths {
        String HomePage = "/home";
        String Login = "/login";
        String Logout = "/logout";
        String Register = "/register";
        String Request = "/request";
        String PersonalCabinet = "/personalCabinet";
        String Admin = "/admin";
        String DeleteBill = "/deleteBill";
        String Bill = "/billDetails";

        List<String> adminUrls = new ArrayList<>();
        List<String> userUrls = new ArrayList<String>() {{
            add(Admin);
        }};
        List<String> anonymousUrls = new ArrayList<String>() {{
            add(Admin);
            add(Request);
            add(PersonalCabinet);
        }};
    }

    interface Attributes {
        String RESULT = "result";
        String ROLE = "role";
        String FORM = "form";
        String USER = "user";
        String CALCULATE_AGAIN = "calculateAgain";
        String BILLS = "bills";
        String ID = "id";
        String BILL = "bill";
    }

    interface Fields {
        String FORM_ID = "formID";
        String NAME = "name";
        String SURNAME = "surname";
        String PATRONYMIC = "patronymic";
        String EMAIL = "email";
        String PHONE = "phone";
        String PASSWORD = "password";
        String CONFIRM_PASSWORD = "confirmPassword";
        String CITY_FROM = "cityFrom";
        String CITY_TO = "cityTo";
        String CITY_TO_COURIER = "cityToCourier";
        String CITY_FROM_COURIER = "cityFromCourier";
        String CITY_FROM_ADDRESS = "cityFromAddress";
        String CITY_TO_ADDRESS = "cityToAddress";
        String WEIGHT = "weight";
        String PACKAGE_TYPE = "packageType";
        String DELIVERY_DATE = "deliveryDate";

    }
    interface Forms {

        interface Login {
            String Id = "loginForm";
            String SubmitButtonText = "form.button.log.in";
            String Title = "form.title.log.in";
            String Action = Paths.Login;
            String Method = HttpMethodEnum.POST.name().toLowerCase();
            String[] Fields = new String[]{PHONE, PASSWORD};

        }
        interface Registration {
            String Id = "registrationForm";
            String SubmitButtonText = "form.button.register";
            String Title = "form.title.register";
            String Action = Paths.Register;
            String Method = HttpMethodEnum.POST.name().toLowerCase();
            String[] Fields = new String[]{PHONE, NAME, SURNAME, PATRONYMIC, EMAIL, PASSWORD, CONFIRM_PASSWORD};

        }
        interface ContactInformation {
            String Id = "contactInformationForm";
            String SubmitButtonText = "form.button.contact.info";
            String Title = "form.title.contact.info";
            String Action = Paths.PersonalCabinet;
            String Method = HttpMethodEnum.POST.name().toLowerCase();
            String[] Fields = new String[]{PHONE, NAME, SURNAME, PATRONYMIC, EMAIL};

        }
        interface Calculate {
            String Id = "calculateForm";
            String SubmitButtonText = "form.button.calculate";
            String Title = "form.title.calculate";
            String Action = Paths.HomePage;
            String Method = HttpMethodEnum.POST.name().toLowerCase();
            String[] Fields = new String[]{CITY_FROM, CITY_FROM_COURIER, CITY_TO, CITY_TO_COURIER, PACKAGE_TYPE, WEIGHT};

        }
        interface Request {
            String Id = "requestForm";
            String SubmitButtonText = "form.button.request";
            String Title = "form.title.request";
            String Action = Paths.Request;
            String Method = HttpMethodEnum.POST.name().toLowerCase();
            String[] Fields = new String[]{CITY_FROM, CITY_FROM_COURIER, CITY_FROM_ADDRESS, CITY_TO, CITY_TO_COURIER, CITY_TO_ADDRESS, PACKAGE_TYPE, WEIGHT, DELIVERY_DATE};
        }

    }
}
