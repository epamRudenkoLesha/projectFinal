package net.polite.deliveryPayment.view.field;

import net.polite.deliveryPayment.view.field.option.Option;

import java.io.Serializable;
import java.util.List;

public class Field implements Serializable{
    private String name;
    private FieldTypeEnum type;
    private FieldInputTypeEnum inputType;
    private String description;
    private String placeholder;
    private String value;
    private String error;
    private String regexp;
    private boolean hasError;
    private boolean mandatory;
    private List<Option> options;
    private boolean hidden;

    Field(String name, FieldTypeEnum type, FieldInputTypeEnum inputType, String description, String placeholder, String regexp, String error, boolean mandatory) {
        this.name = name;
        this.type = type;
        this.inputType = inputType;
        this.description = description;
        this.placeholder = placeholder;
        this.regexp = regexp;
        this.error = error;
        this.mandatory = mandatory;
    }

    Field(String name, FieldTypeEnum type, FieldInputTypeEnum inputType, String value, boolean hidden) {
        this.name = name;
        this.type = type;
        this.inputType = inputType;
        this.value = value;
        this.hidden = hidden;
    }

    Field(String name, FieldTypeEnum type, String description, List<Option> options) {
        this.name = name;
        this.type = type;
        this.description = description;
        this.options = options;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FieldTypeEnum getType() {
        return type;
    }

    public void setType(FieldTypeEnum type) {
        this.type = type;
    }

    public FieldInputTypeEnum getInputType() {
        return inputType;
    }

    public void setInputType(FieldInputTypeEnum inputType) {
        this.inputType = inputType;
    }

    public List getOptions() {
        return options;
    }

    public void setOptions(List options) {
        this.options = options;
    }

    public String getRegexp() {
        return regexp;
    }

    public void setRegexp(String regexp) {
        this.regexp = regexp;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }
}
