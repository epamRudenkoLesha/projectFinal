package net.polite.deliveryPayment.view.field;

import net.polite.deliveryPayment.model.entities.City;
import net.polite.deliveryPayment.model.entities.PackageType;
import net.polite.deliveryPayment.service.CityService;
import net.polite.deliveryPayment.service.PackageTypeService;
import net.polite.deliveryPayment.service.impl.CityServiceImpl;
import net.polite.deliveryPayment.service.impl.PackageTypeServiceImpl;
import net.polite.deliveryPayment.service.impl.ServiceFactory;
import net.polite.deliveryPayment.utils.PropertiesProvider;
import net.polite.deliveryPayment.view.SelectEnum;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.field.option.converters.City2OptionConverter;
import net.polite.deliveryPayment.view.field.option.Option;
import net.polite.deliveryPayment.view.field.option.converters.PackageType2OptionConverter;
import net.polite.deliveryPayment.view.field.option.converters.Select2OptionConverter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import java.util.stream.Collectors;

import static net.polite.deliveryPayment.view.ViewConstants.Fields.*;

public class FieldFactory {

    private CityService cityService = (CityService) ServiceFactory.getInstance().<City>getService(CityServiceImpl.class);
    private PackageTypeService packageTypeService = (PackageTypeService) ServiceFactory.getInstance().<PackageType>getService(PackageTypeServiceImpl.class);
    private Properties properties = PropertiesProvider.getInstance().getProperties();

    public static FieldFactory getInstance() {
        return new InstanceHolder().INSTANCE;
    }

    private static class InstanceHolder {
        FieldFactory INSTANCE = new FieldFactory();
    }

    public Field createField(String fieldName) {
        Field field = null;
        switch (fieldName) {
            case NAME:
                field = getName();
                break;
            case SURNAME:
                field = getSurname();
                break;
            case PATRONYMIC:
                field = getPatronymic();
                break;
            case EMAIL:
                field = getEmail();
                break;
            case PHONE:
                field = getPhone();
                break;
            case PASSWORD:
                field = getPassword();
                break;
            case CONFIRM_PASSWORD:
                field = getConfirmPassword();
                break;
            case CITY_FROM:
                field = getCityFrom();
                break;
            case CITY_TO:
                field = getCityTo();
                break;
            case CITY_TO_COURIER:
                field = getCityToCourier();
                break;
            case CITY_FROM_COURIER:
                field = getCityFromCourier();
                break;
            case CITY_FROM_ADDRESS:
                field = getCityFromAddress();
                break;
            case CITY_TO_ADDRESS:
                field = getCityToAddress();
                break;
            case WEIGHT:
                field = getWeight();
                break;
            case PACKAGE_TYPE:
                field = getPackageType();
                break;
            case DELIVERY_DATE:
                field = getDeliveryDate();
                break;
        }
        return field;
    }

    public Field getFormIdField(String formId) {
        return new Field(ViewConstants.Fields.FORM_ID, FieldTypeEnum.INPUT, FieldInputTypeEnum.HIDDEN, formId, false);
    }

    private Field getName() {
        return new Field(ViewConstants.Fields.NAME, FieldTypeEnum.INPUT, FieldInputTypeEnum.TEXT,
                "field.name.description", "field.name.placeholder", properties.getProperty("field.name.regexp"), "field.name.error", true);
    }

    private Field getSurname() {
        return new Field(ViewConstants.Fields.SURNAME, FieldTypeEnum.INPUT, FieldInputTypeEnum.TEXT,
                "field.surname.description", "field.surname.placeholder", properties.getProperty("field.surname.regexp"), "field.surname.error", false);
    }

    private Field getPatronymic() {
        return new Field(ViewConstants.Fields.PATRONYMIC, FieldTypeEnum.INPUT, FieldInputTypeEnum.TEXT,
                "field.patronymic.description", "field.patronymic.placeholder", properties.getProperty("field.surname.regexp"), null, false);
    }

    private Field getEmail() {
        return new Field(ViewConstants.Fields.EMAIL, FieldTypeEnum.INPUT, FieldInputTypeEnum.TEXT,
                "field.email.description", "field.email.placeholder", properties.getProperty("field.email.regexp"), "field.email.error", true);
    }

    private Field getPhone() {
        return new Field(ViewConstants.Fields.PHONE, FieldTypeEnum.INPUT, FieldInputTypeEnum.TEXT,
                "field.phone.description", "field.phone.placeholder", properties.getProperty("field.phone.regexp"), "field.phone.error", true);
    }

    private Field getPassword() {
        return new Field(ViewConstants.Fields.PASSWORD, FieldTypeEnum.INPUT, FieldInputTypeEnum.PASSWORD,
                "field.password.description", null, properties.getProperty("field.password.regexp"), "field.password.error", true);
    }

    private Field getConfirmPassword() {
        return new Field(ViewConstants.Fields.CONFIRM_PASSWORD, FieldTypeEnum.INPUT, FieldInputTypeEnum.PASSWORD,
                "field.confirmPassword.description", null, properties.getProperty("field.password.regexp"), "field.confirmPassword.error", true);
    }

    private Field getWeight() {
        return new Field(ViewConstants.Fields.WEIGHT, FieldTypeEnum.INPUT, FieldInputTypeEnum.NUMBER,
                "field.weight.description", "field.weight.placeholder", properties.getProperty("field.weight.regexp"), "field.weight.error", true);
    }

    private Field getCityFromAddress() {
        return new Field(ViewConstants.Fields.CITY_FROM_ADDRESS, FieldTypeEnum.INPUT, FieldInputTypeEnum.TEXT,
                "field.cityFrom.address.description", "field.cityFrom.address.placeholder", null, null, false);
    }

    private Field getCityToAddress() {
        return new Field(ViewConstants.Fields.CITY_TO_ADDRESS, FieldTypeEnum.INPUT, FieldInputTypeEnum.TEXT,
                "field.cityTo.address.description", "field.cityTo.address.placeholder", null, null, false);
    }

    private Field getCityFrom() {
        return new Field(ViewConstants.Fields.CITY_FROM, FieldTypeEnum.SELECT, "field.cityFrom.description", getCityOptions());
    }

    private Field getCityTo() {
        return new Field(ViewConstants.Fields.CITY_TO, FieldTypeEnum.SELECT, "field.cityTo.description", getCityOptions());
    }

    private Field getCityFromCourier() {
        return new Field(ViewConstants.Fields.CITY_FROM_COURIER, FieldTypeEnum.SELECT, "field.cityFrom.courier.description", getSelectOptions());
    }

    private Field getCityToCourier() {
        return new Field(ViewConstants.Fields.CITY_TO_COURIER, FieldTypeEnum.SELECT, "field.cityTo.courier.description", getSelectOptions());
    }

    private Field getPackageType() {
        return new Field(ViewConstants.Fields.PACKAGE_TYPE, FieldTypeEnum.SELECT, "field.packageType.description", getPackageTypeOptions());
    }

    private Field getDeliveryDate() {
        Field field = new Field(ViewConstants.Fields.DELIVERY_DATE, FieldTypeEnum.INPUT, FieldInputTypeEnum.DATE,
                "field.delivery.date.description", null, properties.getProperty("field.date.regexp"), "field.delivery.date.error", true);
        DateFormat df = new SimpleDateFormat(ViewConstants.DateFormat);
        field.setValue(df.format(new Date()));
        return field;
    }

    private List<Option> getCityOptions() {
        List<City> cities = cityService.getAll();
        cities.sort(Comparator.comparing(City::getName));
        return cities.stream().map(City2OptionConverter::convert).collect(Collectors.toList());
    }

    private List<Option> getSelectOptions() {
        List<SelectEnum> selectEnums = new ArrayList<>(Arrays.asList(SelectEnum.values()));
        selectEnums.sort(Comparator.comparingInt(SelectEnum::getId));
        return selectEnums.stream().map(Select2OptionConverter::convert).collect(Collectors.toList());
    }

    private List<Option> getPackageTypeOptions() {
        List<PackageType> packageTypes = packageTypeService.getAll();
        packageTypes.sort(Comparator.comparing(PackageType::getId));
        return packageTypes.stream().map(PackageType2OptionConverter::convert).collect(Collectors.toList());
    }
}