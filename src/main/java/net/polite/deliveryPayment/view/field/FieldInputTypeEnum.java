package net.polite.deliveryPayment.view.field;

public enum FieldInputTypeEnum {
    TEXT,
    NUMBER,
    HIDDEN,
    PASSWORD,
    DATE
}
