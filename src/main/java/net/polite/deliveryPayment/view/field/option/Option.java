package net.polite.deliveryPayment.view.field.option;

import java.io.Serializable;

public class Option implements Serializable{
    private String value;
    private String placeholder;

    public Option(String value, String placeholder) {
        this.value = value;
        this.placeholder = placeholder;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }
}
