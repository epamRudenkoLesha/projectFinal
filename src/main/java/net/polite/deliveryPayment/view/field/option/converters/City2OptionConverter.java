package net.polite.deliveryPayment.view.field.option.converters;

import net.polite.deliveryPayment.model.entities.City;
import net.polite.deliveryPayment.view.field.option.Option;

public class City2OptionConverter {

    public static Option convert(City city) {
        return new Option("" + city.getIsoCode(), city.getName());
    }
}
