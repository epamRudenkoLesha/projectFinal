package net.polite.deliveryPayment.view.field.option.converters;

import net.polite.deliveryPayment.model.entities.PackageType;
import net.polite.deliveryPayment.view.field.option.Option;

public class PackageType2OptionConverter {

    public static Option convert(PackageType packageType) {
        return new Option("" + packageType.getId(), packageType.getName());
    }
}
