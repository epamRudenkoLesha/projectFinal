package net.polite.deliveryPayment.view.field.option.converters;

import net.polite.deliveryPayment.view.SelectEnum;
import net.polite.deliveryPayment.view.field.option.Option;

public class Select2OptionConverter {

    public static Option convert(SelectEnum sel) {
        return new Option(sel.getCode(), sel.getLocalizedName());
    }
}
