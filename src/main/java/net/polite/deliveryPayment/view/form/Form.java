package net.polite.deliveryPayment.view.form;

import net.polite.deliveryPayment.view.field.Field;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Form implements Serializable{

    private String id;
    private String action;
    private String method;
    private String buttonText;
    private String title;
    private boolean hasErrors;
    private List<Field> fields;
    private List<String> errors = new ArrayList<>();

    public Form(String id, String action, String method, String buttonText, List<Field> fields, String title) {
        this.id = id;
        this.action = action;
        this.method = method;
        this.buttonText = buttonText;
        this.fields = fields;
        this.title = title;
        hasErrors = false;
    }

    public void addError(String error) {
        errors.add(error);
    }

    public boolean getHasErrors() {
        return hasErrors;
    }

    public void setHasErrors(boolean hasErrors) {
        this.hasErrors = hasErrors;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
