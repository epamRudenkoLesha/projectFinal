package net.polite.deliveryPayment.view.form;

import net.polite.deliveryPayment.view.field.Field;
import net.polite.deliveryPayment.view.field.FieldFactory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static net.polite.deliveryPayment.view.ViewConstants.Forms.Login;
import static net.polite.deliveryPayment.view.ViewConstants.Forms.Calculate;
import static net.polite.deliveryPayment.view.ViewConstants.Forms.Registration;
import static net.polite.deliveryPayment.view.ViewConstants.Forms.Request;
import static net.polite.deliveryPayment.view.ViewConstants.Forms.ContactInformation;

public class FormFactory {

    private static class InstanceHolder {

        private static FormFactory INSTANCE = new FormFactory();
    }
    private FieldFactory fieldFactory = FieldFactory.getInstance();
    public static FormFactory getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private Form createForm(String formId, String action, String method, String[] formFields, String buttonText, String title) {
        List<Field> fields = Arrays.stream(formFields)
                .map(field -> fieldFactory.createField(field)).collect(Collectors.toList());
        fields.add(fieldFactory.getFormIdField(formId));
        return new Form(formId, action, method, buttonText, fields, title);
    }

    public Form getLoginForm() {
        return createForm(Login.Id, Login.Action, Login.Method, Login.Fields, Login.SubmitButtonText, Login.Title);
    }

    public Form getRegistrationForm() {
        return createForm(Registration.Id, Registration.Action, Registration.Method, Registration.Fields, Registration.SubmitButtonText, Registration.Title);
    }

    public Form getCalculateForm() {
        return createForm(Calculate.Id, Calculate.Action, Calculate.Method, Calculate.Fields, Calculate.SubmitButtonText, Calculate.Title);
    }

    public Form getRequestForm() {
        return createForm(Request.Id, Request.Action, Request.Method, Request.Fields, Request.SubmitButtonText, Request.Title);
    }

    public Form getContactInfoForm() {
        return createForm(ContactInformation.Id, ContactInformation.Action, ContactInformation.Method, ContactInformation.Fields, ContactInformation.SubmitButtonText, ContactInformation.Title);
    }
}
