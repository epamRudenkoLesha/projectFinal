package net.polite.deliveryPayment.view.form;

import net.polite.deliveryPayment.model.dao.jdbc.DaoFactoryJDBC;
import net.polite.deliveryPayment.view.ViewConstants;
import net.polite.deliveryPayment.view.field.Field;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormValidator {

    public static final String ERROR_PASSWORD_BLANK = "error.password.blank";
    public static final String ERROR_PASSWORD_NOT_EQUAL = "error.password.not.equal";
    public static Logger LOG = LogManager.getLogger(DaoFactoryJDBC.class);


    public static void validate(Form form) {
        for (Field field : form.getFields()) {
            if (Objects.nonNull(field.getRegexp())) {
                Pattern p = Pattern.compile(field.getRegexp());
                Matcher m = p.matcher(field.getValue());
                boolean valid = m.matches();
                if (!valid) {
                    form.setHasErrors(true);
                    field.setHasError(true);
                }
            }
        }
    }

    public static void validatePasswords(Form form) {
        Optional<Field> pass = form.getFields().stream()
                .filter(f1 -> f1.getName().equals(ViewConstants.Fields.PASSWORD)).findFirst();
        Optional<Field> confirmPass = form.getFields().stream()
                .filter(f1 -> f1.getName().equals(ViewConstants.Fields.CONFIRM_PASSWORD)).findFirst();
        if (pass.isPresent() && confirmPass.isPresent()) {
            String password = pass.get().getValue();
            String confirmPassword = confirmPass.get().getValue();
            if (!Strings.isBlank(password) && !Strings.isBlank(confirmPassword)) {
                if (!password.equals(confirmPassword)) {
                    form.setHasErrors(true);
                    form.addError(ERROR_PASSWORD_NOT_EQUAL);
                }
            } else {
                form.setHasErrors(true);
                form.addError(ERROR_PASSWORD_BLANK);
            }

        } else {
            LOG.warn("Form {} has no password or/and confirm password field.", form.getId());
        }
    }

    public static void validateExistingUser(Form form) {
        //todo
    }
}
