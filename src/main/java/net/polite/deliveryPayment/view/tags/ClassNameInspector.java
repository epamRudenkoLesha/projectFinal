package net.polite.deliveryPayment.view.tags;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Util class that helps finding field in specified declared class or in inner class.
 */
public class ClassNameInspector {

    /**
     * The constant LOG.
     */
    public static Logger LOG = LogManager.getLogger(ClassNameInspector.class);

    /**
     * Gets field from declared class name.
     *
     * @param fieldName         the field name
     * @param declaredClassName the declared class name
     * @return the field
     * @throws NoSuchFieldException the no such field exception
     */
    public static Field getField(String fieldName, String declaredClassName) throws NoSuchFieldException {
        Class<?> aClass;
        try {
            aClass = Class.forName(declaredClassName);
        } catch (ClassNotFoundException e) {
            PathParser parser = new PathParser(declaredClassName);
            String innerClassName = parser.getName();
            declaredClassName = parser.getDeclaringClassName();
            aClass = getInnerClass(innerClassName, declaredClassName);
        }
        return aClass.getField(fieldName);
    }

    private static Class<?> getInnerClass(String innerClassName, String declaredClassName) {
        Class aClass = null;
        try {
            aClass = Class.forName(declaredClassName);
            List<Class> declaredClasses = Arrays.asList(aClass.getDeclaredClasses());
            Optional<Class> any = declaredClasses.stream().filter(c -> c.getSimpleName().equals(innerClassName)).findAny();
            if (any.isPresent()) {
                aClass = any.get();
            } else {
                throw new ClassNotFoundException();
            }
        } catch (ClassNotFoundException e) {
            LOG.error("No such class {} found!", declaredClassName);
        }
        return aClass;
    }
}
