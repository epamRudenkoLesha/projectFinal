package net.polite.deliveryPayment.view.tags;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.taglibs.standard.tag.common.core.UrlSupport;

import javax.servlet.jsp.JspException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class PathConstantTag extends UrlSupport {

    public static Logger LOG = LogManager.getLogger(PathConstantTag.class);
    private String value;

    public PathConstantTag() {
        super();
    }

    @Override
    public int doStartTag() throws JspException {
        PathParser parser = new PathParser(this.value);
        String fieldName = parser.getName();
        String declaringClassName = parser.getDeclaringClassName();
        try {
            Field field = ClassNameInspector.getField(fieldName, declaringClassName);
            if (!Modifier.isPublic(field.getModifiers()) ||
                    !Modifier.isStatic(field.getModifiers()) ||
                    !Modifier.isFinal(field.getModifiers())) {
                throw new IllegalArgumentException("Field " + fieldName + " is not a public static final field");
            }
            super.value = (String) field.get(null);
        } catch (IllegalAccessException e) {
            LOG.error("Field {} in class {} has private access!", fieldName, declaringClassName);
        } catch (NoSuchFieldException e) {
            LOG.error("No field {} found in class {}!", fieldName, declaringClassName);
        }
        return super.doStartTag();
    }


    public void setValue(String value) {
        this.value = value;
    }
}
