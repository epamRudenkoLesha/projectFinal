package net.polite.deliveryPayment.view.tags;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A utility class that parses the simple field name (or inner class name) and its declaring class
 * from the fully qualified name (path).
 */

public class PathParser {

    private static final String REGEX_FIELD_PATH = "((\\w+\\.)+)(\\w+)";
    private static final int GROUP_INDEX_CLASS = 1;
    private static final int GROUP_INDEX_FIELD = 3;

    private String className;
    private String name;

    /**
     * Constructor for the parser. The path name is parsed into
     * simple filed name (or inner class name) and its declaring class name.
     *
     * @param path the fully qualified name (path)
     */
    public PathParser(String path) {
        Matcher matcher = Pattern.compile(REGEX_FIELD_PATH).matcher(path);
        matcher.find();
        this.className = matcher.group(GROUP_INDEX_CLASS);
        this.className = className.substring(0, className.length() - 1);
        this.name = matcher.group(GROUP_INDEX_FIELD);
    }

    public String getDeclaringClassName() {
        return this.className;
    }

    public String getName() {
        return this.name;
    }
}
