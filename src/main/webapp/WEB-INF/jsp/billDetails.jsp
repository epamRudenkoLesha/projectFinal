<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/customTag.tld" %>

<fmt:setBundle basename="localization.packageDelivery"/>

<ct:path value="net.polite.deliveryPayment.view.ViewConstants.Paths.HomePage" var="homepage"/>
<ct:path var="delete" value="net.polite.deliveryPayment.view.ViewConstants.Paths.DeleteBill"/>
<ct:path var="pay" value="net.polite.deliveryPayment.view.ViewConstants.Paths.Bill"/>

<html>
<head>
    <title><fmt:message key="page.title.login"/></title>
</head>
<body>

<jsp:include page="header.jsp"/>
<form action="${delete}" method="post">
    <input type="hidden" name="id" value="${bill.id}">
    <input type="submit" value="<fmt:message key="bill.delete"/> ">
</form>
<table>
    <tr>
        <td width="250"><fmt:message key="bill.city.from"/></td>
        <td>${bill.distance.cityFrom.name}</td>
    </tr>
    <tr>
        <td width="250"><fmt:message key="field.cityFrom.courier.description"/></td>
        <td><fmt:message key="${bill.courierFrom? 'select.yes' : 'select.no'}"/></td>
    </tr>
    <tr>
        <td width="250"><fmt:message key="field.cityFrom.address.description"/></td>
        <td>${bill.addressFrom}</td>
    </tr>
    <tr>
        <td width="250"><fmt:message key="bill.city.to"/></td>
        <td>${bill.distance.cityTo.name}</td>
    </tr>
    <tr>
        <td width="250"><fmt:message key="field.cityTo.courier.description"/></td>
        <td><fmt:message key="${bill.courierTo ? 'select.yes' : 'select.no'}"/></td>
    </tr>
    <tr>
        <td width="250"><fmt:message key="field.cityTo.address.description"/></td>
        <td>${bill.addressTo}</td>
    </tr>
    <tr>
        <td width="250"><fmt:message key="bill.package.type"/></td>
        <td>${bill.aPackage.packageType.name}</td>
    </tr>
    <tr>
        <td width="250"><fmt:message key="bill.package.weight"/></td>
        <td>${bill.aPackage.weight} <fmt:message key="weight.gramm"/></td>
    </tr>
    <tr>
        <td width="250"><fmt:message key="bill.cost"/></td>
        <td>${bill.cost} <fmt:message key="currency.grn"/> </fmt></td>
    </tr>
    <tr>
        <td width="250"><fmt:message key="bill.package.deliveryDate"/></td>
        <td>${bill.aPackage.deliveryDate}</td>
    </tr>
</table>


<form action="${pay}" method="post">
    <input type="hidden" name="id" value="${bill.id}">
    <input type="submit" value="<fmt:message key="bill.pay"/> ">
</form>
<br>
<a href="${homepage}"><fmt:message key="link.return.home.page"/></a><br>
<hr>
</body>
</html>