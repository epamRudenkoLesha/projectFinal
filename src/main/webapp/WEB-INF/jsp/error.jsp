<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/customTag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setBundle basename="localization.packageDelivery"/>

<ct:path value="net.polite.deliveryPayment.view.ViewConstants.Paths.HomePage" var="homepage"/>

<html>
<head>
    <title><fmt:message key="page.title.error"/></title>
</head>
<body>
<jsp:include page="header.jsp"/>

<div class="fig">
<pre class="fig" contenteditable="true">





    eeeeeeeeeeee    rrrrr   rrrrrrrrr   rrrrr   rrrrrrrrr      ooooooooooo   rrrrr   rrrrrrrrr
  ee::::::::::::ee  r::::rrr:::::::::r  r::::rrr:::::::::r   oo:::::::::::oo r::::rrr:::::::::r
 e::::::eeeee:::::eer:::::::::::::::::r r:::::::::::::::::r o:::::::::::::::or:::::::::::::::::r
e::::::e     e:::::err::::::rrrrr::::::rrr::::::rrrrr::::::ro:::::ooooo:::::orr::::::rrrrr::::::r
e:::::::eeeee::::::e r:::::r     r:::::r r:::::r     r:::::ro::::o     o::::o r:::::r     r:::::r
e:::::::::::::::::e  r:::::r     rrrrrrr r:::::r     rrrrrrro::::o     o::::o r:::::r     rrrrrrr
e::::::eeeeeeeeeee   r:::::r             r:::::r            o::::o     o::::o r:::::r
e:::::::e            r:::::r             r:::::r            o::::o     o::::o r:::::r
e::::::::e           r:::::r             r:::::r            o:::::ooooo:::::o r:::::r
 e::::::::eeeeeeee   r:::::r             r:::::r            o:::::::::::::::o r:::::r
  ee:::::::::::::e   r:::::r             r:::::r             oo:::::::::::oo  r:::::r
    eeeeeeeeeeeeee   rrrrrrr             rrrrrrr               ooooooooooo    rrrrrrr


                                                                                                 </pre>
</div>
<hr>
<a href="${homepage}"><fmt:message key="link.return.home.page"/></a>

</body>
</html>
