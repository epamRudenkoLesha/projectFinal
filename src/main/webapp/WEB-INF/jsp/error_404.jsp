<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/customTag.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setBundle basename="localization.packageDelivery"/>

<ct:path value="net.polite.deliveryPayment.view.ViewConstants.Paths.HomePage" var="homepage"/>

<html>
<head>
    <title><fmt:message key="page.title.error_404"/></title>
</head>
<body>
<jsp:include page="header.jsp"/>

<div class="fig">
    <pre class="fig" contenteditable="true">

       444444444         000000000              444444444
      4::::::::4       00:::::::::00           4::::::::4
     4:::::::::4     00:::::::::::::00        4:::::::::4
    4::::44::::4    0:::::::000:::::::0      4::::44::::4
   4::::4 4::::4    0::::::0   0::::::0     4::::4 4::::4
  4::::4  4::::4    0:::::0     0:::::0    4::::4  4::::4
 4::::4   4::::4    0:::::0     0:::::0   4::::4   4::::4
4::::444444::::444  0:::::0 000 0:::::0  4::::444444::::444
4::::::::::::::::4  0:::::0 000 0:::::0  4::::::::::::::::4
4444444444:::::444  0:::::0     0:::::0  4444444444:::::444
          4::::4    0:::::0     0:::::0            4::::4
          4::::4    0::::::0   0::::::0            4::::4
          4::::4    0:::::::000:::::::0            4::::4
        44::::::44   00:::::::::::::00           44::::::44
        4::::::::4     00:::::::::00             4::::::::4
        4444444444       000000000               4444444444


                                                       </pre>

</div>
<hr>
<a href="${homepage}"><fmt:message key="link.return.home.page"/></a>
</body>
</html>
