<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/customTag.tld" %>
<%@ page import="net.polite.deliveryPayment.utils.LanguageEnum" %>
<%@ page import="net.polite.deliveryPayment.model.entities.RoleEnum" %>

<fmt:setBundle basename="localization.packageDelivery"/>
<ct:path value="net.polite.deliveryPayment.view.ViewConstants.Paths.Login" var="login"/>
<ct:path value="net.polite.deliveryPayment.view.ViewConstants.Paths.Logout" var="logout"/>
<ct:path value="net.polite.deliveryPayment.view.ViewConstants.Paths.PersonalCabinet" var="personalCabinet"/>

<table>

    <th style="width: 20%"></th>
    <th style="width: 60%"></th>
    <th style="width: 20%"></th>
    <tr>
        <td>
            <form>
                <select id="language" name="lang" onchange="submit()">
                    <option value="${LanguageEnum.ENG}" ${lang == 'eng' ? 'selected' : ''}>English</option>
                    <option value="${LanguageEnum.UKR}" ${lang == 'ukr' ? 'selected' : ''}>Українська</option>
                    <option value="${LanguageEnum.RUS}" ${lang == 'rus' ? 'selected' : ''}>Русский</option>
                </select>
            </form>
        </td>
        <td>
            <c:if test="${role ne RoleEnum.ANONYMOUS}">
                <fmt:message key="message.welcome"/> ${user.name} ${" "} ${user.surname}
            </c:if>
        </td>
        <td>
            <c:choose>
                <c:when test="${role eq RoleEnum.ANONYMOUS}">
                    <a href="${login}"><fmt:message key="link.log.in"/></a>
                </c:when>
                <c:otherwise>
                    <a href="${personalCabinet}"><fmt:message key="link.personal.cabinet"/></a><br>
                    <a href="${logout}"><fmt:message key="link.log.out"/></a>
                </c:otherwise>
            </c:choose>
        </td>
    </tr>
</table>
<hr>

