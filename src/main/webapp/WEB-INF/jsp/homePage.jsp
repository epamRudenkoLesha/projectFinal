<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/customTag.tld" %>

<fmt:setBundle basename="localization.packageDelivery"/>
<ct:path value="net.polite.deliveryPayment.view.ViewConstants.Paths.HomePage" var="homepage"/>
<html>
<head>
    <title><fmt:message key="page.title.homepage"/></title>
</head>
<body>

<jsp:include page="header.jsp"/>

<c:if test="${not empty form}">
    <tags:form form="${form}"/>
</c:if>
<c:if test="${not empty result}">
    <fmt:message key="message.delivery.cost"/> ${result} <fmt:message key="currency.grn"/>
</c:if>
<hr>
<c:if test="${calculateAgain}">
    <a href="${homepage}"><fmt:message key="link.calculate.again"/></a><br>
</c:if>
</body>
</html>
