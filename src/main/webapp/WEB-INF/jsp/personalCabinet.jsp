<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/customTag.tld" %>

<fmt:setBundle basename="localization.packageDelivery"/>

<ct:path value="net.polite.deliveryPayment.view.ViewConstants.Paths.HomePage" var="homepage"/>

<html>
<head>
    <title><fmt:message key="page.title.personal.cabinet"/></title>
</head>
<body>

<jsp:include page="header.jsp"/>

<c:if test="${not empty form}">
    <tags:form form="${form}"/>
</c:if>
<br>
<br>

<c:if test="${not empty bills}">
    <tags:billsList bills="${bills}"/>
</c:if>

<hr>
<a href="${homepage}"><fmt:message key="link.return.home.page"/></a><br>
</body>
</html>