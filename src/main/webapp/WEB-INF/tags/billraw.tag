<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>
<%@ taglib prefix="ct" uri="/WEB-INF/tld/customTag.tld" %>

<%@ attribute name="bill" required="true" type="net.polite.deliveryPayment.model.entities.Bill" %>

<fmt:setBundle basename="localization.packageDelivery"/>
<ct:path var="delete" value="net.polite.deliveryPayment.view.ViewConstants.Paths.DeleteBill"/>
<ct:path var="getBill" value="net.polite.deliveryPayment.view.ViewConstants.Paths.Bill"/>

<td>${bill.distance.cityFrom.name}</td>
<td>${bill.distance.cityTo.name}</td>
<td>${bill.aPackage.packageType.name}</td>
<td>${bill.aPackage.weight} <fmt:message key="weight.gramm"/></td>
<td>${bill.cost} <fmt:message key="currency.grn"/></td>
<td>${bill.aPackage.deliveryDate}</td>
<td>
    <c:if test="${bill.paid}">
        <fmt:message key="message.bill.paid.true"/>
    </c:if>
    <c:if test="${not bill.paid}">
        <fmt:message key="message.bill.paid.false"/>
    </c:if>
</td>
<td>${bill.paidDate}</td>
<c:if test="${not bill.paid}">
    <td>
        <form action="${delete}" method="post">
            <input type="hidden" name="id" value="${bill.id}">
            <input type="submit" value="<fmt:message key="bill.delete"/> ">
        </form>
    </td>
    <td>
        <form action="${getBill}" method="get">
            <input type="hidden" name="id" value="${bill.id}">
            <input type="submit" value="<fmt:message key="bill.get"/> ">
        </form>
    </td>
</c:if>