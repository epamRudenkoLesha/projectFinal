<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>

<%@ attribute name="bills" required="true" type="java.util.List" %>
<fmt:setBundle basename="localization.packageDelivery"/>

<h4><fmt:message key="bill.list.title"/></h4>
<table>
    <tr>
        <td width="150"><fmt:message key="bill.city.from"/></td>
        <td width="150"><fmt:message key="bill.city.to"/></td>
        <td width="100"><fmt:message key="bill.package.type"/></td>
        <td width="100"><fmt:message key="bill.package.weight"/></td>
        <td width="100"><fmt:message key="bill.cost"/></td>
        <td width="150"><fmt:message key="bill.package.deliveryDate"/></td>
        <td width="150"><fmt:message key="bill.paid"/></td>
        <td width="150"><fmt:message key="bill.paid.date"/></td>
    </tr>
    <c:forEach items="${bills}" var="item">
        <tr>
            <tags:billraw bill="${item}"/>
        </tr>
    </c:forEach>
</table>
