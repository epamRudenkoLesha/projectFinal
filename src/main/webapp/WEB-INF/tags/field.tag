<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ tag import="net.polite.deliveryPayment.view.field.FieldTypeEnum" %>
<%@ tag import="net.polite.deliveryPayment.view.field.FieldInputTypeEnum" %>
<%@ attribute name="field" required="true" type="net.polite.deliveryPayment.view.field.Field" %>

<fmt:setBundle basename="localization.packageDelivery"/>

<table>
    <tr>
        <th width="250"/>
        <th width="100"/>
        <th/>

    </tr>
    <tr>
        <td>
            <c:if test="${not empty field.description}">
                <fmt:message key="${field.description}"/>
            </c:if>


        </td>
        <td>
            <c:if test="${field.type == FieldTypeEnum.SELECT}">
                <select name="${field.name}">
                    <c:forEach items="${field.options}" var="optionValue">
                        <option value="${optionValue.value}" ${field.value == optionValue.value? 'selected' : ''}>
                                ${optionValue.placeholder}
                        </option>
                    </c:forEach>
                </select>
            </c:if>
            <c:if test="${field.type == FieldTypeEnum.INPUT}">
                <input type="${field.inputType}"
                       name="${field.name}"
                        <c:if test="${not empty field.placeholder}">
                            placeholder="<fmt:message key="${field.placeholder}"/>"
                        </c:if>
                        <c:if test="${not empty field.value}">
                            value="${field.value}"
                        </c:if>
                       <c:if test="${field.inputType == FieldInputTypeEnum.NUMBER}">step="1"</c:if>
                    ${field.hidden? 'hidden':''}
                />

            </c:if>
            <c:if test="${field.mandatory}">
                *
            </c:if>
        </td>
        <td>
            <c:if test="${field.hasError}">
                <fmt:message key="${field.error}"/>
            </c:if>
        </td>
    </tr>
</table>