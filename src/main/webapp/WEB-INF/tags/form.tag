<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>

<%@ attribute name="form" required="true" type="net.polite.deliveryPayment.view.form.Form" %>
<fmt:setBundle basename="localization.packageDelivery"/>

<form action="${form.action}" method="${form.method}">
    <h3><fmt:message key="${form.title}"/></h3>
    <c:if test="${form.hasErrors}">
        <c:forEach items="${form.errors}" var="error">
            <fmt:message key="${error}"/>
        </c:forEach>
    </c:if>
    <c:forEach items="${form.fields}" var="field">
        <tags:field field="${field}"/>
    </c:forEach>
    <input type="submit" value="<fmt:message key="${form.buttonText}"/>"/><br/>
</form>


