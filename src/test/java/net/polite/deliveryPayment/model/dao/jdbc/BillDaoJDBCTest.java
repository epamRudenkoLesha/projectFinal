package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.BillDao;
import net.polite.deliveryPayment.model.dao.RequestDao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.*;
import net.polite.deliveryPayment.model.entities.Package;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class BillDaoJDBCTest {

    public static final String ADDRESS_FROM = "testAddressFrom";
    public static final String ADDRESS_TO = "testAddressTo";

    @Before
    public void importDataSet() throws Exception {
        DaoSuiteJDBCTest.setDataSet("db.test.initTable.request", DatabaseOperation.CLEAN_INSERT);
    }

    @Test
    public void findBillById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            BillDao dao = (BillDao) DaoFactoryJDBC.getInstance().<Bill>getDao(BillDaoJDBC.class, connection);
            Bill bill = dao.get(4);
            assertThat(bill.getDistance().getId(), is(2));
            assertThat(bill.getaPackage().getId(), is(1));
            assertThat(bill.getUser().getId(), is(2));
            assertThat(bill.isCourierFrom(), is(true));
            assertThat(bill.isCourierTo(), is(true));
            assertThat(bill.getAddressFrom(), is("просп. Перемоги 2"));
            assertThat(bill.getAddressTo(), is("Туринська 5"));
        }
    }

    @Test
    public void updateBill() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            BillDao dao = (BillDao) DaoFactoryJDBC.getInstance().<Bill>getDao(BillDaoJDBC.class, connection);
            Date date = new Date();
            Bill requestToSave = new Bill(1,null,null, null,
                    true,true, null, null, 45,  date,true);
            Bill bill = dao.update(requestToSave);

            List<Bill> list = dao.getAll();
            assertEquals(6, list.size());

            assertThat(bill.isPaid(), is(true));
            assertThat(bill.getPaidDate().getYear(), is(date.getYear()));
            assertThat(bill.getPaidDate().getMonth(), is(date.getMonth()));
            assertThat(bill.getPaidDate().getDay(), is(date.getDay()));
        }
    }

    @Test(expected = QueryException.class)
    public void findNoBillById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            BillDao dao = (BillDao) DaoFactoryJDBC.getInstance().<Bill>getDao(BillDaoJDBC.class, connection);
            Bill bill = dao.get(400);
        }
    }

    @Test
    public void findAll() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            BillDao dao = (BillDao) DaoFactoryJDBC.getInstance().<Bill>getDao(BillDaoJDBC.class, connection);
            List<Bill> requests = dao.getAll();
            assertEquals(6, requests.size());
        }
    }

    @Test
    public void findAllForUser() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            BillDao dao = (BillDao) DaoFactoryJDBC.getInstance().<Bill>getDao(BillDaoJDBC.class, connection);
            List<Bill> requests = dao.getAll(new User(1));
            assertEquals(3, requests.size());
        }
    }



}