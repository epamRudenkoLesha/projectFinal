package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.CityDao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.City;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class CityDaoJDBCTest {
    @Before
    public void importDataSet() throws Exception {
        DaoSuiteJDBCTest.setDataSet("db.test.initTable.city", DatabaseOperation.CLEAN_INSERT);
    }

    @Test
    public void findCityById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            CityDao cityDao = (CityDao) DaoFactoryJDBC.getInstance().<City>getDao(CityDaoJDBC.class, connection);
            City city = cityDao.get(1);
            assertThat(city.getCode(), is("city.name.vin"));
            assertThat(city.getIsoCode(), is(1));
            assertThat(city.getCourierTarif(), is(3000));
        }
    }

    @Test
    public void findAll() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            CityDao cityDao = (CityDao) DaoFactoryJDBC.getInstance().<City>getDao(CityDaoJDBC.class, connection);
            List<City> cities = cityDao.getAll();
            assertEquals(9, cities.size());
        }
    }

    @Test
    public void findCityByIsoCode() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            CityDao cityDao = (CityDao) DaoFactoryJDBC.getInstance().<City>getDao(CityDaoJDBC.class, connection);
            City city = cityDao.getByIsoCode(7);
            assertThat(city.getCode(), is("city.name.kyiv"));
            assertThat(city.getId(), is(7));
            assertThat(city.getCourierTarif(), is(4000));
        }
    }

    @Test(expected = QueryException.class)
    public void findNotExistedCityByID() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            CityDao cityDao = (CityDao) DaoFactoryJDBC.getInstance().<City>getDao(CityDaoJDBC.class, connection);
            City city = cityDao.get(10);
        }
    }

    @Test(expected = QueryException.class)
    public void findNotExistedCityByIso() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            CityDao cityDao = (CityDao) DaoFactoryJDBC.getInstance().<City>getDao(CityDaoJDBC.class, connection);
            City city = cityDao.getByIsoCode(12);
        }
    }
}