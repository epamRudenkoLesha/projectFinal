package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapperImpl;
import net.polite.deliveryPayment.utils.PropertiesProvider;
import org.dbunit.*;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        DistanceDaoJDBCTest.class,
        UserDaoJDBCTest.class,
        CityDaoJDBCTest.class,
        PackageTypeDaoJDBCTest.class,
        PackageDaoJDBCTest.class,
        RequestDaoJDBCTest.class,
        BillDaoJDBCTest.class
})
public class DaoSuiteJDBCTest {

    private static Properties projectProperties = PropertiesProvider.getInstance().getProperties();
    public static final String DRIVER = projectProperties.getProperty("db.test.driver");
    public static final String PASS = projectProperties.getProperty("db.test.password");
    public static final String USER = projectProperties.getProperty("db.test.userName");
    public static final String URL = projectProperties.getProperty("db.test.url");

    @BeforeClass
    public static void setUp() throws Exception {
        Class.forName(DRIVER);
        try (Connection connection = getConnection().getConnection()) {
            String fileName = projectProperties.getProperty("db.test.initialize");
            InputStreamReader in = new InputStreamReader(UserDaoJDBCTest.class.getResourceAsStream(fileName));
            RunScript.execute(connection, in);
        }
    }

    static void setDataSet(String property, DatabaseOperation databaseOperation) throws Exception {
        String fileName = PropertiesProvider.getInstance().getProperty(property);
        InputStream populateStream = UserDaoJDBCTest.class.getResourceAsStream(fileName);
        IDatabaseTester databaseTester = getDataBaseTester();
        IDataSet dataSet = new FlatXmlDataSetBuilder().setColumnSensing(true).build(populateStream);
        databaseTester.setSetUpOperation(databaseOperation);
        databaseTester.setDataSet(dataSet);
        databaseTester.getConnection().getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new H2DataTypeFactory());
        databaseTester.onSetup();
    }

    static ConnectionWrapper getConnection() throws SQLException {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL(URL);
        dataSource.setUser(USER);
        dataSource.setPassword(PASS);
        return new ConnectionWrapperImpl(dataSource.getConnection());
    }

    static IDatabaseTester getDataBaseTester() throws ClassNotFoundException {
        return new JdbcDatabaseTester(DRIVER, URL, USER, PASS);
    }

    @AfterClass
    public static void tearDown() {
    }
}
