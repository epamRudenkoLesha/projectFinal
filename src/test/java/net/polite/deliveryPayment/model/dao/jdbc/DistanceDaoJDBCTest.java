package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.DistanceDao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.Distance;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class DistanceDaoJDBCTest {

    @Before
    public void importDataSet() throws Exception {
        DaoSuiteJDBCTest.setDataSet("db.test.initTable.distance", DatabaseOperation.CLEAN_INSERT);
    }

    @Test
    public void findDistanceById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            DistanceDao distanceDao = (DistanceDao) DaoFactoryJDBC.getInstance().<Distance>getDao(DistanceDaoJDBC.class, connection);
            Distance distance = distanceDao.get(111);
            assertThat(distance.getCityFrom().getIsoCode(), is(2));
            assertThat(distance.getCityTo().getIsoCode(), is(1));
            assertThat(distance.getDistance(), is(645));
        }
    }

    @Test
    public void findAll() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            DistanceDao distanceDao = (DistanceDao) DaoFactoryJDBC.getInstance().<Distance>getDao(DistanceDaoJDBC.class, connection);
            List<Distance> distances = distanceDao.getAll();
            assertEquals(8, distances.size());
        }
    }

    @Test
    public void findDistanceByIsoCodes() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            DistanceDao distanceDao = (DistanceDao) DaoFactoryJDBC.getInstance().<Distance>getDao(DistanceDaoJDBC.class, connection);
            Distance distance = distanceDao.findByCitiesIso(3, 1);
            assertThat(distance.getId(), is(211));
            assertThat(distance.getCityFrom().getIsoCode(), is(3));
            assertThat(distance.getCityTo().getIsoCode(), is(1));
            assertThat(distance.getDistance(), is(868));
        }
    }

    @Test(expected = QueryException.class)
    public void failToFindDistanceByIsoCodes() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            DistanceDao distanceDao = (DistanceDao) DaoFactoryJDBC.getInstance().<Distance>getDao(DistanceDaoJDBC.class, connection);
            Distance distance = distanceDao.findByCitiesIso(3, 3);
        }
    }

    @Test(expected = QueryException.class)
    public void failToFindDistanceById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            DistanceDao distanceDao = (DistanceDao) DaoFactoryJDBC.getInstance().<Distance>getDao(DistanceDaoJDBC.class, connection);
            Distance distance = distanceDao.get(3);
        }
    }

}