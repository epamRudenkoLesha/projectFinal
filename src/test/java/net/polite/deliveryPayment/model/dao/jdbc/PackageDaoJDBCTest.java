package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.PackageDao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.Package;
import net.polite.deliveryPayment.model.entities.PackageType;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class PackageDaoJDBCTest {
    @Before
    public void importDataSet() throws Exception {
        DaoSuiteJDBCTest.setDataSet("db.test.initTable.package", DatabaseOperation.CLEAN_INSERT);
    }

    @Test
    public void findPackageById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            PackageDao dao = (PackageDao) DaoFactoryJDBC.getInstance().<Package>getDao(PackageDaoJDBC.class, connection);
            Package aPackage = dao.get(11);
            assertThat(aPackage.getWeight(), is(2000));
            assertThat(aPackage.getPackageType().getId(), is(11));
        }
    }

    @Test
    public void saveUser() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            PackageDao dao = (PackageDao) DaoFactoryJDBC.getInstance().<Package>getDao(PackageDaoJDBC.class, connection);
            PackageType packagetype = new PackageType(11);
            Date deliveryDate = new Date();
            Package aPackage = new Package(0, 1234, deliveryDate, packagetype);
            int save = dao.save(aPackage);
            Package result = dao.get(save);
            assertThat(result.getWeight(), is(1234));
            assertThat(result.getPackageType().getId(), is(11));
            assertThat(result.getDeliveryDate().getYear(), is(deliveryDate.getYear()));
            assertThat(result.getDeliveryDate().getMonth(), is(deliveryDate.getMonth()));
            assertThat(result.getDeliveryDate().getDay(), is(deliveryDate.getDay()));
            List<Package> packages = dao.getAll();
            assertEquals(6, packages.size());

        }
    }

    @Test(expected = QueryException.class)
    public void findNoPackageById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            PackageDao dao = (PackageDao) DaoFactoryJDBC.getInstance().<Package>getDao(PackageDaoJDBC.class, connection);
            Package aPackage = dao.get(101);
        }
    }

    @Test
    public void findAll() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            PackageDao dao = (PackageDao) DaoFactoryJDBC.getInstance().<Package>getDao(PackageDaoJDBC.class, connection);
            List<Package> packageTypes = dao.getAll();
            assertEquals(5, packageTypes.size());
        }
    }

}