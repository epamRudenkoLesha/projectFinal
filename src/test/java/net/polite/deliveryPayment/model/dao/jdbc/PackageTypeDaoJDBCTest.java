package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.PackageTypeDao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.PackageType;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class PackageTypeDaoJDBCTest {
    @Before
    public void importDataSet() throws Exception {
        DaoSuiteJDBCTest.setDataSet("db.test.initTable.packageType", DatabaseOperation.CLEAN_INSERT);
    }

    @Test
    public void findPackageTypeById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            PackageTypeDao dao = (PackageTypeDao) DaoFactoryJDBC.getInstance().<PackageType>getDao(PackageTypeDaoJDBC.class, connection);
            PackageType packageType = dao.get(12);
            assertThat(packageType.getTariff(), is(100));
            assertThat(packageType.getCodeValue(), is("package.type.normal"));
        }
    }

    @Test(expected = QueryException.class)
    public void findNoPackageTypeById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            PackageTypeDao dao = (PackageTypeDao) DaoFactoryJDBC.getInstance().<PackageType>getDao(PackageTypeDaoJDBC.class, connection);
            PackageType packageType = dao.get(102);
        }
    }

    @Test
    public void findAll() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            PackageTypeDao dao = (PackageTypeDao) DaoFactoryJDBC.getInstance().<PackageType>getDao(PackageTypeDaoJDBC.class, connection);
            List<PackageType> packageTypes = dao.getAll();
            assertEquals(2, packageTypes.size());
        }
    }

}