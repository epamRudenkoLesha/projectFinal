package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.RequestDao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.Distance;
import net.polite.deliveryPayment.model.entities.Request;
import net.polite.deliveryPayment.model.entities.Package;
import net.polite.deliveryPayment.model.entities.User;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class RequestDaoJDBCTest {

    public static final String ADDRESS_FROM = "testAddressFrom";
    public static final String ADDRESS_TO = "testAddressTo";

    @Before
    public void importDataSet() throws Exception {
        DaoSuiteJDBCTest.setDataSet("db.test.initTable.request", DatabaseOperation.CLEAN_INSERT);
    }

    @Test
    public void findRequestById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            RequestDao dao = (RequestDao) DaoFactoryJDBC.getInstance().<Request>getDao(RequestDaoJDBC.class, connection);
            Request request = dao.get(4);
            assertThat(request.getDistance().getId(), is(2));
            assertThat(request.getaPackage().getId(), is(1));
            assertThat(request.getUser().getId(), is(2));
            assertThat(request.isCourierFrom(), is(true));
            assertThat(request.isCourierTo(), is(true));
            assertThat(request.getAddressFrom(), is("просп. Перемоги 2"));
            assertThat(request.getAddressTo(), is("Туринська 5"));
        }
    }

    @Test
    public void saveRequest() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            RequestDao dao = (RequestDao) DaoFactoryJDBC.getInstance().<Request>getDao(RequestDaoJDBC.class, connection);
            Distance distance = new Distance(1);
            Package apackage = new Package(1);
            User user = new User(4);
            Request requestToSave = new Request(0,distance,apackage, user, true,false, ADDRESS_FROM, ADDRESS_TO, 133);
            int save = dao.save(requestToSave);
            List<Request> list = dao.getAll();
            assertEquals(7, list.size());

            Request request = dao.get(save);
            assertThat(request.getDistance().getId(), is(1));
            assertThat(request.getaPackage().getId(), is(1));
            assertThat(request.getUser().getId(), is(4));
            assertThat(request.isCourierFrom(), is(true));
            assertThat(request.isCourierTo(), is(false));
            assertThat(request.getAddressFrom(), is(ADDRESS_FROM));
            assertThat(request.getAddressTo(), is(ADDRESS_TO));
        }
    }

    @Test(expected = QueryException.class)
    public void findNoRequestById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            RequestDao dao = (RequestDao) DaoFactoryJDBC.getInstance().<Request>getDao(RequestDaoJDBC.class, connection);
            Request request = dao.get(400);
        }
    }

    @Test
    public void findAll() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            RequestDao dao = (RequestDao) DaoFactoryJDBC.getInstance().<Request>getDao(RequestDaoJDBC.class, connection);
            List<Request> requests = dao.getAll();
            assertEquals(6, requests.size());
        }
    }



}