package net.polite.deliveryPayment.model.dao.jdbc;

import net.polite.deliveryPayment.model.dao.UserDao;
import net.polite.deliveryPayment.model.dao.connection.ConnectionWrapper;
import net.polite.deliveryPayment.model.dao.exception.QueryException;
import net.polite.deliveryPayment.model.entities.RoleEnum;
import net.polite.deliveryPayment.model.entities.User;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class UserDaoJDBCTest {

    public static final String NAME = "Test";
    public static final String SURNAME = "TestS";
    public static final String PATRONYMIC = "TestP";
    public static final String EMAIL = "test@test.com";
    public static final String PHONE = "0001112233";
    public static final String PASSWORD = "test";

    @Before
    public void importDataSet() throws Exception {
        DaoSuiteJDBCTest.setDataSet("db.test.initTable.user", DatabaseOperation.CLEAN_INSERT);
    }

    @Test
    public void findUserById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            UserDao dao = (UserDao) DaoFactoryJDBC.getInstance().<User>getDao(UserDaoJDBC.class, connection);
            User user = dao.get(11);
            assertThat(user.getName(), is("Олексій"));
            assertThat(user.getSurname(), is("Потапенко"));
            assertThat(user.getEmail(), is("potapenko@gmail.com"));
            assertThat(user.getPhone(), is("+380998887766"));
            assertThat(user.getPassword(), is("Парарам"));
        }
    }

    @Test
    public void findUserByPhone() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            UserDao dao = (UserDao) DaoFactoryJDBC.getInstance().<User>getDao(UserDaoJDBC.class, connection);
            User user = dao.findUser("+380998887766");
            assertThat(user.getId(), is(11));
            assertThat(user.getName(), is("Олексій"));
            assertThat(user.getSurname(), is("Потапенко"));
            assertThat(user.getEmail(), is("potapenko@gmail.com"));
            assertThat(user.getPhone(), is("+380998887766"));
            assertThat(user.getPassword(), is("Парарам"));
        }
    }

    @Test
    public void saveUser() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            UserDao dao = (UserDao) DaoFactoryJDBC.getInstance().<User>getDao(UserDaoJDBC.class, connection);
            User userToSave = new User(0, NAME, SURNAME, PATRONYMIC, EMAIL, PHONE, PASSWORD);

            int save = dao.save(userToSave);
            User user = dao.get(save);
            assertThat(user.getName(), is(NAME));
            assertThat(user.getSurname(), is(SURNAME));
            assertThat(user.getPatronymic(), is(PATRONYMIC));
            assertThat(user.getEmail(), is(EMAIL));
            assertThat(user.getPhone(), is(PHONE));
            assertThat(user.getPassword(), is(PASSWORD));
            List<User> users = dao.getAll();
            assertEquals(5, users.size());
        }
    }

    @Test
    public void updateUser() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            UserDao dao = (UserDao) DaoFactoryJDBC.getInstance().<User>getDao(UserDaoJDBC.class, connection);
            User userToUpdate = new User(11, NAME, SURNAME, PATRONYMIC, EMAIL, PHONE, PASSWORD);
            User result = dao.update(userToUpdate);
            assertThat(result.getName(), is(NAME));
            assertThat(result.getSurname(), is(SURNAME));
            assertThat(result.getPatronymic(), is(PATRONYMIC));
            assertThat(result.getEmail(), is(EMAIL));
            assertThat(result.getPhone(), is(PHONE));
            List<User> users = dao.getAll();
            assertEquals(4, users.size());
        }
    }

    @Test
    public void addRoleToUser() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            UserDao dao = (UserDao) DaoFactoryJDBC.getInstance().<User>getDao(UserDaoJDBC.class, connection);
            User userToSave = new User(11);
            dao.addRoleToUser(userToSave, RoleEnum.ADMIN);
            List<Integer> ids = dao.getUserRoles(userToSave.getId());
            assertEquals(2, ids.size());
        }
    }

    @Test(expected = QueryException.class)
    public void failToAddRoleToUser() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            UserDao dao = (UserDao) DaoFactoryJDBC.getInstance().<User>getDao(UserDaoJDBC.class, connection);
            User userToSave = new User(1);
            dao.addRoleToUser(userToSave, RoleEnum.ADMIN);
        }
    }

    @Test(expected = QueryException.class)
    public void findNoUserById() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            UserDao dao = (UserDao) DaoFactoryJDBC.getInstance().<User>getDao(UserDaoJDBC.class, connection);
            User user = dao.get(101);
        }
    }

    @Test
    public void findAll() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            UserDao dao = (UserDao) DaoFactoryJDBC.getInstance().<User>getDao(UserDaoJDBC.class, connection);
            List<User> users = dao.getAll();
            assertEquals(4, users.size());
        }
    }

    @Test
    public void findUserRoles() throws Exception {
        try (ConnectionWrapper connection = DaoSuiteJDBCTest.getConnection()) {
            UserDao dao = (UserDao) DaoFactoryJDBC.getInstance().<User>getDao(UserDaoJDBC.class, connection);
            List<Integer> ids = dao.getUserRoles(41);
            assertEquals(2, ids.size());
        }
    }

}